################################################################################
# Package: LoKiFitters
################################################################################
gaudi_subdir(LoKiFitters)

gaudi_depends_on_subdirs(Kernel/LHCbMath
                         Phys/DaVinciKernel
                         Phys/DecayTreeFitter
                         Phys/KalmanFilter
                         Phys/LoKiPhys)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(LoKiFitters
                 src/*.cpp
                 LINK_LIBRARIES LHCbMathLib DaVinciKernelLib DecayTreeFitter KalmanFilter LoKiPhysLib)

