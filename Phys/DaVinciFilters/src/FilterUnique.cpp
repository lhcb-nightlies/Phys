// ============================================================================
// Include files
// ============================================================================
//  STD& STL 
// ============================================================================
#include <map>
#include <set>
#include <algorithm>
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Primitives.h"
#include "LoKi/PhysTypes.h"
#include "LoKi/IHybridFactory.h"
#include "LoKi/PhysExtract.h"
// ============================================================================
// local
// ============================================================================
#include "FilterDesktop.h"
// ============================================================================
/** @class FilterUnique
 *  Simple variation of FilterDesktop algorithm, that allows to
 *  remove explicit duplicates (the decayus where mother particles 
 *  share the same daughetr particles)
 *  @code
 *  from PhysConf.Selections import MergedSelection, SimpleSelection 
 *  detached_jpsi = ...
 *  highpt_jpsi   = ...
 *  all_jpsi      = MergedSelection ( 'AllJpsi' , 
 *    RequiredSelections = [ detached_jspi , highpt_jpsi ] )
 *  # eliminate duplicates 
 *  unique_jpsi   = SimpleSelection ( 
 *      'UniqueJpsi'  , 
 *      all_jpsi      , 
 *      FilyerUnique  , 
 *      Code = "DECTREE('Meson -> mu+ mu-')"
 *      )      
 *  @endcode
 *  Or (much better) 
 *  @code
 *  from PhysConf.Selections import MergedSelection 
 *  detached_jpsi = ...
 *  highpt_jpsi   = ...
 *  all_jpsi      = MergedSelection ( 'AllJpsi' , 
 *    RequiredSelections = [ detached_jspi , highpt_jpsi ] , 
 *    Code = "DECTREE('Meson -> mu+ mu-')" , 
 *    Unique = True ) 
 *  @endcode 
 *  @see FilterDesktop
 *  @author Vanya BELYAEV   Ivan.Belyaev@itep.ru
 *  @date 2017-04-03
 */
class FilterUnique : public FilterDesktop 
{
  // ==========================================================================
  /// friend factory for instantiation
  friend class AlgFactory<FilterUnique> ;
  // ==========================================================================
protected:
  // ==========================================================================
  /** standard constructor
   *  @see FilterDesktop
   *  @see DVAlgorithm
   *  @see GaudiTupleAlg
   *  @see GaudiHistoAlg
   *  @see GaudiAlgorithm
   *  @see Algorithm
   *  @see AlgFactory
   *  @see IAlgFactory
   *  @param name the algorithm instance name
   *  @param pSvc pointer to Service Locator
   */
  FilterUnique                                 // standard contructor
  ( const std::string& name ,                  // the algorithm instance name
    ISvcLocator*       pSvc )                  // pointer to Service Locator
    : FilterDesktop ( name , pSvc ) 
  {}
  // ==========================================================================
  /// virtual & protected destructor
  virtual ~FilterUnique() {} ;                // virtual & protected destructor
  // ==========================================================================
public:
  // ==========================================================================
  /** the major method for filter input particles
   *  @param input    (INPUT) the input  container of particles
   *  @param filtered (OUPUT) the output container of particles
   *  @return Status code
   */
  StatusCode filter
  ( const LHCb::Particle::ConstVector& input    ,
    LHCb::Particle::ConstVector&       filtered ) override
  {
    //
    LHCb::Particle::ConstVector  good  ;
    good.reserve ( input.size() ) ;
    //
    /// remove obvious duplicates
    std::set<const LHCb::Particle*> iset ( input.begin() , input.end() ) ;
    //
    // Filter particles!!
    std::copy_if ( iset.begin () ,
                   iset.end   () ,
                   std::back_inserter ( good ) , 
                   std::cref ( predicate() ) ) ;
    //
    // find less obvious duplicates
    typedef  std::map<const LHCb::Particle*,LHCb::Particle::ConstVector> MAP ;
    MAP lmap ;
    //
    for ( const LHCb::Particle *p : good ) 
    {
      if ( nullptr == p ) { continue ; }
      //
      LHCb::Particle::ConstVector d ; d.reserve ( 7 ) ;
      LoKi::Extract::particles ( p->daughters().begin()   , 
                                 p->daughters().end  ()   , 
                                 std::back_inserter ( d ) ) ;
      std::sort ( d.begin() , d.end() ) ;
      //
      lmap[p] = d ;
    }
    //
    for ( MAP::const_iterator i = lmap.begin() ; lmap.end() != i ; ++i ) 
    {
      const LHCb::Particle* p1 = i->first ;
      //
      bool unique = true ;
      MAP::const_iterator ii = i ; ++ii ;
      for ( MAP::const_iterator j = ii ; lmap.end() != j ; ++j ) 
      {
        if ( i->second.size() != j->second.size() ) { continue ; } // CONTINUE 
        if ( std::equal ( i->second.begin() ,  i->second.end () , 
                          j->second.begin()                     ) ) 
        {
          unique = false ; break ;                                // BREAK 
        }
      }
      if ( unique ) { filtered.push_back ( p1 ) ; }
    }
    //
    markParticles ( filtered ) ;
    //
    // some countings
    StatEntity& cnt = counter ( "efficiency" ) ;
    //
    const size_t size1 = filtered  . size () ;
    const size_t size2 = good      . size () ;
    //
    for ( size_t i1 = 0     ; size1 > i1 ; ++i1 ) { cnt += true  ; }
    for ( size_t i2 = size1 ; size2 > i2 ; ++i2 ) { cnt += false ; }
    //
    return SUCCESS ;
  }
  // ==========================================================================
private:
  // ==========================================================================
  /// the default constructor is disabled
  FilterUnique () ;                       // the default consructor is disabled
  /// copy constructor is disabled
  FilterUnique ( const FilterUnique& ) ;        // copy constructor is disabled
  /// assignement operator is disabled
  FilterUnique& operator=( const FilterUnique& ) ;            // no assignement
  // ==========================================================================
} ;
// ============================================================================  
/// the factory
DECLARE_ALGORITHM_FACTORY(FilterUnique)
// ============================================================================
//  The END 
// ============================================================================
