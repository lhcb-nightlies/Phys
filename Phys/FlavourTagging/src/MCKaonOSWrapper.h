#ifndef MCKAONOSWRAPPER_H
#define MCKAONOSWRAPPER_H 1

#include "TMVAWrapper.h"

namespace MyMCKaonOSSpace { class Read_kaonMLPBNN_MC; }

class MCKaonOSWrapper : public TMVAWrapper {
public:
	MCKaonOSWrapper(std::vector<std::string> &);
	~MCKaonOSWrapper();
	double GetMvaValue(std::vector<double> const &) override;

private:
	MyMCKaonOSSpace::Read_kaonMLPBNN_MC * reader;

};

#endif
