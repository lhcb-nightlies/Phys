#ifndef MCELECTRONOSWRAPPER_H
#define MCELECTRONOSWRAPPER_H 1

#include "TMVAWrapper.h"

namespace MyMCElectronOSSpace { class Read_eleMLPBNN_MC; }

class MCElectronOSWrapper : public TMVAWrapper {
public:
	MCElectronOSWrapper(std::vector<std::string> &);
	~MCElectronOSWrapper();
	double GetMvaValue(std::vector<double> const &) override;

private:
	MyMCElectronOSSpace::Read_eleMLPBNN_MC * reader;

};

#endif
