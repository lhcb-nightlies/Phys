#ifndef ELECTRONOSWRAPPER_H
#define ELECTRONOSWRAPPER_H 1

#include "TMVAWrapper.h"

namespace MyElectronOSSpace { class Read_eleMLPBNN; }

class ElectronOSWrapper : public TMVAWrapper {
public:
	ElectronOSWrapper(std::vector<std::string> &);
	~ElectronOSWrapper();
	double GetMvaValue(std::vector<double> const &) override;

private:
	MyElectronOSSpace::Read_eleMLPBNN * reader;

};

#endif
