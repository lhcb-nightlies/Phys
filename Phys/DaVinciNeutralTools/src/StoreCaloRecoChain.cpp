// Include files 

 // from Gaudi
#include "GaudiKernel/AlgFactory.h"
// local
#include "StoreCaloRecoChain.h"
#include "CaloUtils/CaloAlgUtils.h"

//-----------------------------------------------------------------------------
// Implementation file for class : StoreCaloRecoChain
//
//
// 2016-12-06 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( StoreCaloRecoChain )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
StoreCaloRecoChain::StoreCaloRecoChain( const std::string& name,
                                        ISvcLocator* pSvcLocator )
: DaVinciAlgorithm ( name , pSvcLocator )
{
  declareProperty("TESprefix"    ,m_prefix="CaloReco");
  declareProperty("UseStatusMask",m_mask=true); //  true : only store the masked digits
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode StoreCaloRecoChain::initialize()
{
  const auto sc = DaVinciAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  m_edata = tool<ICaloDataProvider>("CaloDataProvider", "EcalDataProvider",this);
  m_pdata = tool<ICaloDataProvider>("CaloDataProvider", "PrsDataProvider",this);
  counterStat = tool<ICounterLevel>("CounterLevel");

  // storage TES naming
  m_locClusters      = m_prefix + "/" + LHCb::CaloClusterLocation::Ecal;
  m_locSplitClusters = m_prefix + "/" + LHCb::CaloClusterLocation::EcalSplit;
  m_locEcalDigits    = m_prefix + "/" + LHCb::CaloDigitLocation::Ecal;
  m_locPrsDigits     = m_prefix + "/" + LHCb::CaloDigitLocation::Prs;
  m_locSpdDigits     = m_prefix + "/" + LHCb::CaloDigitLocation::Spd;
  m_locEcalAdcs      = m_prefix + "/" + LHCb::CaloAdcLocation::Ecal;
  m_locPrsAdcs       = m_prefix + "/" + LHCb::CaloAdcLocation::Prs;
  // Counting
  return sc;
}

//========================================
StatusCode StoreCaloRecoChain::execute()
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // Input particles
  const LHCb::Particle::Range parts = particles();

  // set true if we have any input particles
  setFilterPassed( !parts.empty() ); 

  if ( !parts.empty() )
  {
    if( msgLevel(MSG::VERBOSE) ) 
      verbose() << " === Found " << parts.size() << " particles in the input container(s) " 
                << inputLocations() << endmsg;

    //== create the containers for the reduced calo-reco chain 
    m_clusters     = getOrCreate<LHCb::CaloClusters,LHCb::CaloClusters>( m_locClusters     );
    m_splitClusters= getOrCreate<LHCb::CaloClusters,LHCb::CaloClusters>( m_locSplitClusters);
    m_edigits      = getOrCreate<LHCb::CaloDigits,LHCb::CaloDigits>    ( m_locEcalDigits   );
    m_pdigits      = getOrCreate<LHCb::CaloDigits,LHCb::CaloDigits>    ( m_locPrsDigits    );
    m_sdigits      = getOrCreate<LHCb::CaloDigits,LHCb::CaloDigits>    ( m_locSpdDigits    );
    m_eadcs        = getOrCreate<LHCb::CaloAdcs,LHCb::CaloAdcs>        ( m_locEcalAdcs     );
    m_padcs        = getOrCreate<LHCb::CaloAdcs,LHCb::CaloAdcs>        ( m_locPrsAdcs      );
      
    const auto _nClus=m_clusters->size() ;
    const auto _nSClus=m_splitClusters->size() ;
    const auto _neDig=m_edigits->size();
    const auto _neAdc=m_eadcs->size()  ;
    const auto _npDig= m_pdigits->size() ;
    const auto _npAdc=m_padcs->size();
    const auto _nsDig=m_sdigits->size();
      
    unsigned int ncalo = 0;
    for ( const auto part : parts )
    {
      // collect calo particles in the tree
      auto caloParts = getCaloTree( part );
      ncalo += caloParts.size();
      if( msgLevel(MSG::VERBOSE) ) 
        verbose() << " == Particle : " << part->particleID().pid() << " => found " << caloParts.size() 
                  << " neutral calo object in the  descendants" << endmsg;
      for( const auto calo : caloParts )
      {
        if( msgLevel(MSG::VERBOSE) )  
          verbose() << "  = Calo Particle : " << calo->particleID().pid() << endmsg;
        m_counter.set();
        storeCalo( calo ); // clone & store the reco chain for each particle
        if ( msgLevel(MSG::VERBOSE) ) m_counter.print();
      }    
    }
    
    // Monitoring :
    if( msgLevel(MSG::VERBOSE) ) 
    {
      info() << " ==== SUMMARY ==== " <<endmsg;
      info() << " === Found " << parts.size() << " particles in the input container " << endmsg;
      info() << "  == Found " << ncalo  << " calo objects in the particles descendants " << endmsg;
      info() << "   = Stored : "  << m_clusters->size() - _nClus <<  " new clusters " << endmsg;
      info() << "   = Stored : "  << m_splitClusters->size()-_nSClus <<  " new split-clusters " << endmsg;
      info() << "   = Stored : "  << m_edigits->size()-_neDig <<  " new Ecal digits " << endmsg;
      info() << "   = Stored : "  << m_eadcs->size()-_neAdc   <<  " new Ecal ADCs   " << endmsg;
      info() << "   = Stored : "  << m_pdigits->size()-_npDig <<  " new Prs digits  " << endmsg;
      info() << "   = Stored : "  << m_padcs->size()-_npAdc   <<  " new Prs ADCs    " << endmsg;
      info() << "   = Stored : "  << m_sdigits->size()-_nsDig <<  " new Spd digits  " << endmsg;
    }
    counter("# CaloReco chains")+=ncalo;
    if(counterStat->isQuiet())
    {
      counter(m_locClusters)      += m_clusters->size() - _nClus;
      counter(m_locSplitClusters) += m_splitClusters->size() - _nSClus ;
      counter(m_locEcalDigits)    += m_edigits->size() - _neDig ;
      counter(m_locPrsDigits)     += m_pdigits->size() - _npDig ;
      counter(m_locEcalAdcs)      += m_eadcs->size() - _neAdc   ;
      counter(m_locPrsAdcs)       += m_padcs->size() - _npAdc   ;
      counter(m_locSpdDigits)     += m_sdigits->size() - _nsDig ;
    }  
  }

  return StatusCode::SUCCESS;
}

//========================================
std::vector<const LHCb::Particle*> StoreCaloRecoChain::getCaloTree( const LHCb::Particle* p )
{
  std::vector<const LHCb::Particle*> tree;
  if ( p->isBasicParticle() && isPureNeutralCalo(p) )
  {
    tree.push_back( p );
  }
  else if ( !p->isBasicParticle() )
  {
    //const auto & daughters = p->daughters(); 
    for ( const auto d : p->daughters() )
    {
      for ( const auto dd : getCaloTree(d) )
      {
        tree.push_back( dd );
      }
    }
  }
  return tree;
}

//========================================
bool StoreCaloRecoChain::storeCalo( const LHCb::Particle* p )
{
  if ( !p ) return false;
  if(  !p->isBasicParticle() || ! isPureNeutralCalo(p) ) return false;
  // get (neutral) protoP
  const auto * proto = p->proto();
  if ( !proto ) return false;
  // get CaloHypo ( (neutral)proto->hypo one-to-one correspondance)
  return ( !proto->calo().empty() ? storeHypo(proto->calo().front()) : false ); 
}

//========================================
bool StoreCaloRecoChain::storeHypo( const LHCb::CaloHypo* h )
{
  if ( !h ) return false;
  // 1-  store the related extra-digits (Prs/Spd)
  const auto & digits  = h->digits();
  const auto nExtraDig = digits.size();
  unsigned int nAddExtraDig = 0;
  for ( const auto d : digits )
  {
    if ( storeDigit(d) ) { ++nAddExtraDig; }
  }
  if( msgLevel(MSG::VERBOSE) ) 
    verbose() << " - Stored :  " << nAddExtraDig << "/" 
              << nExtraDig << " new extra Digits" << endmsg;

  // 2- in case of merged pi0 -  store the associated split-clusters :
  if ( h -> hypothesis() == LHCb::CaloHypo::Pi0Merged )
  {
    for ( const auto& split : h->hypos() )
    {
      storeCluster( LHCb::CaloAlgUtils::ClusterFromHypo(split) , m_splitClusters );
    }
  }

  // 3- store the main cluster
  const bool storeClus = 
    storeCluster( LHCb::CaloAlgUtils::ClusterFromHypo(h) , m_clusters );
  if( msgLevel(MSG::VERBOSE) ) 
    verbose() << " - Stored " << storeClus << " new CaloCluster" << endmsg;
  return storeClus;
}

//========================================
bool StoreCaloRecoChain::storeCluster( const LHCb::CaloCluster* c,  
                                       LHCb::CaloClusters* clusters )
{
  if ( !c || !clusters ) { return false; }
  const auto id = c->seed();
  // check (by cellID) whether the corresponding cluster is already stored or not
  for ( const auto cc : *clusters )
  {
    if ( cc->seed() == id )
    {
      if ( msgLevel(MSG::VERBOSE) ) 
        verbose()<<"  -- calocluster has already been stored" << endmsg;
      return false;
    } 
  }
  if ( clusters->object(c->key()) ) return false;
  // if not clone & store the cluster
  auto * pCluster = c->clone();  
  clusters->insert( pCluster, c->key() );
  m_counter("Cluster");
  // ... and store the related Ecal digits
  const auto & entries = c->entries();
  unsigned int nMaskedDigits(0), nStoredDigits(0);
  std::vector<LHCb::CaloClusterEntry> clonedEntries;
  clonedEntries.reserve( entries.size() );
  for ( const auto& e : entries )
  {
    if ( m_mask && ( e.status() & m_status ) == 0 ) continue;
    ++nMaskedDigits;
    auto * newDig = storeDigit(e.digit());
    if ( newDig ) 
    {
      ++nStoredDigits; 
      clonedEntries.emplace_back( e );
      clonedEntries.back().setDigit( newDig );
    }
  }
  pCluster->entries().clear();
  pCluster->setEntries( clonedEntries );
  if( msgLevel(MSG::VERBOSE) ) 
    verbose()<<" - Stored " << nStoredDigits 
             << " new CaloDigits (#Masked ="<<nMaskedDigits<<"/#Entries="
             <<pCluster->entries().size()<<")"<<endmsg;
  
  return true;
}

const LHCb::CaloDigit* StoreCaloRecoChain::storeDigit( const LHCb::CaloDigit* d )
{
  if ( !d ) return nullptr;
  const auto id = d->cellID();
  // assign the right container
  LHCb::CaloDigits * digits = nullptr;
  if(m_ecal(d ))    { digits = m_edigits; }
  else if(m_prs(d)) { digits = m_pdigits; }
  else if(m_spd(d)) { digits = m_sdigits; }
  else              { return nullptr; }  
  // check (by cellID) whether the corresponding digit is already stored or not
  auto *         pDigit = digits->object(id);
  if ( !pDigit ) pDigit = digits->object(d->key());
  if ( pDigit ) return pDigit;
  pDigit = d->clone();
  digits->insert( pDigit, d->key() ); // clone & store the digit
  if(m_ecal(d ))m_counter("Ecal Digits");
  if(m_prs(d ))m_counter("Prs Digits");
  if(m_spd(d ))m_counter("Spd Digits");
  // ... and store the corresponding ADC
  storeADC( d );
  return pDigit;
}

bool StoreCaloRecoChain::storeADC( const LHCb::CaloDigit* d )
{
  if ( !d ) return false;
  if ( m_spd( d )) return false; // no need to store ADC for the binary SPD
  const auto id = d->cellID();
  // assign the right container
  LHCb::CaloAdcs* adcs = nullptr;
  if(m_ecal(d))     { adcs = m_eadcs; }
  else if(m_prs(d)) { adcs = m_padcs; }
  else              { return false;   }
  // == check (by cellID) whether the corresponding ADC is already stored or not
  if ( adcs->object( id ) ) return false;
  const int adc = (m_ecal(d)) ? m_edata->adc( id , -255 ) : m_pdata->adc( id , 0 );
  if ( m_ecal(d) ) m_counter("Ecal ADC");
  if ( m_prs(d)  ) m_counter("Prs ADC");
  adcs->insert( new LHCb::CaloAdc( id, adc ), id ); 
  return true;
}
