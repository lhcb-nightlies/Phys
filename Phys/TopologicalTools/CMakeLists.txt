################################################################################
# Package: TopologicalTools
################################################################################
gaudi_subdir(TopologicalTools)

gaudi_depends_on_subdirs(Phys/DaVinciKernel
                         Event/HltEvent)

find_package(ROOT)
find_package(Boost)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_module(TopologicalTools
                 src/*.cpp
                 LINK_LIBRARIES DaVinciKernelLib HltEvent)
