// ============================================================================
// Include files 
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/IAlgContextSvc.h"
#include "GaudiKernel/SmartIF.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GetAlgs.h"
// ============================================================================
// local
// ============================================================================
#include "LoKi/Particles47.h"
#include "LoKi/ParticleProperties.h"
// ============================================================================
namespace 
{
  // ==========================================================================
  /// the default name for the tool 
  const std::string s_toolname = 
    "ANNGlobalPID::ChargedProtoANNPIDTool/ChargedProtoANNPID:PUBLIC" ;
  const  ANNGlobalPID::IChargedProtoANNPIDTool* const s_tool = nullptr  ;
  /// list of electron names 
  const std::vector<std::string> s_e = {{
      "ELECTRON" , "E" , "E+" , "E-" , 
      "Electron" , "e" , "e+" , "e-" , 
      "electron" }} ;    
  /// list of muon names 
  const std::vector<std::string> s_mu = {{
      "MUON" , "MUON+" , "MUON-" , "MU" , "MU+" , "MU-"  ,
      "Muon" , "Muon+" , "Muon-" , "Mu" , "Mu+" , "Mu-"  ,
      "muon" , "muon+" , "muon-" , "mu" , "mu+" , "mu-"  }} ;
  /// list of pion names 
  const std::vector<std::string> s_pi = {{
      "PION" , "PION+" , "PION-" , "PI" , "PI+" , "PI-"  ,
      "Pion" , "Pion+" , "Pion-" , "Pi" , "Pi+" , "Pi-"  ,
      "pion" , "pion+" , "pion-" , "pi" , "pi+" , "pi-"  }} ;
  /// list of kaon names 
  const std::vector<std::string> s_K  = {{
      "KAON" , "KAON+" , "KAON-" , "PI" , "PI+" , "PI-"  ,
      "Kaon" , "Kaon+" , "Kaon-" , "K"  , "K+"  , "K-"   ,
      "kaon" , "kaon+" , "kaon-" , "k"  , "k+"  , "k-"  }} ;
  /// list of proton names 
  const std::vector<std::string> s_P  = {{
      "PROTON" , "PROTON+" , "PROTON-" , "P" , "P+" , "P-"  ,
      "Proton" , "Proton+" , "Proton-" , "p" , "p+" , "p-"  ,
      "proton" , "proton+" , "proton-" } } ;
  /// list of ghost names 
  const std::vector<std::string> s_gh = {{
      "GHOST"  , "Ghost" , "ghost" , "" }} ;
  // ==========================================================================
}
// ============================================================================
/*  constructor from particle type & tune 
 *  @param ptype the  type of particle 
 *  @param tune  tune 
 */
// ============================================================================
LoKi::Particles::ANNPID::ANNPID
( const std::string& ptype , 
  const std::string&  tune ) 
  : LoKi::Particles::ANNPID::ANNPID ( ptype , tune , s_toolname ) 
{}
// ============================================================================
// mandatory: virtual destructor 
// ============================================================================
LoKi::Particles::ANNPID::~ANNPID()
{ if ( m_tool && !gaudi() ) { m_tool.reset() ; } }
// ============================================================================
// MANDATORY: clone method ("virtual contructor")
// ============================================================================
LoKi::Particles::ANNPID*
LoKi::Particles::ANNPID::clone   () const 
{ return new LoKi::Particles::ANNPID(*this) ; }
// ============================================================================
/*  constructor from particle type, tune & tool 
 *  @param ptype the  type of particle 
 *  @param tune  tune 
 *  @param tool  typename for the tool 
 */
// ============================================================================
LoKi::Particles::ANNPID::ANNPID
( const std::string& ptype , 
  const std::string& tune  , 
  const std::string& tool  ) 
  :  LoKi::AuxFunBase ( std::tie ( ptype ,  tune , tool ) ) 
  ,  LoKi::BasicFunctors<const LHCb::Particle*>::Function ()
  , m_type     ( ptype  ) 
  , m_tune     ( tune   ) 
  , m_toolname ( tool   )
  , m_pid      ( 0      )
  , m_tool     ( s_tool ) 
{
  //
  if      ( s_e .end() != std::find ( s_e .begin() , s_e .end() , ptype ) ) { m_type = "e+"  ; }
  else if ( s_mu.end() != std::find ( s_mu.begin() , s_mu.end() , ptype ) ) { m_type = "mu+" ; }
  else if ( s_pi.end() != std::find ( s_pi.begin() , s_pi.end() , ptype ) ) { m_type = "pi+" ; }
  else if ( s_K .end() != std::find ( s_K .begin() , s_K .end() , ptype ) ) { m_type = "K+"  ; }
  else if ( s_P .end() != std::find ( s_P .begin() , s_P .end() , ptype ) ) { m_type = "p+"  ; }
  else if ( s_gh.end() != std::find ( s_gh.begin() , s_gh.end() , ptype ) ) { m_type = ""    ; }
  else { Exception ( "Unknown particle type '" + ptype + "'" ) ; }
  //
  if ( "" != m_type )  
  {
    m_pid = LoKi::Particles::pidFromName ( m_type ) ;
    Assert ( 0 !=  m_pid.pid() , "Invalid particle type '" + m_type + "'" ) ;  
    m_pid = LHCb::ParticleID ( m_pid.abspid() ) ;
  }
  // now need to get the tool 
  const LoKi::ILoKiSvc* svc0 = lokiSvc() ;
  Assert ( nullptr  != svc0 , "Can't get LoKi service!" );
  LoKi::ILoKiSvc* svc = const_cast<LoKi::ILoKiSvc*> ( svc0 ) ;
  // first try to use context  stuff 
  // get the context service: 
  SmartIF<IAlgContextSvc> cntx ( svc ) ;  
  if ( cntx )
  {
    // 2. get 'simple' algorithm from the context:
    GaudiAlgorithm* alg = Gaudi::Utils::getGaudiAlg ( cntx ) ;
    if ( 0 != alg  ) 
    {
      // get the tool from the algorithm
      const ANNGlobalPID::IChargedProtoANNPIDTool* pid = 
        alg -> tool<ANNGlobalPID::IChargedProtoANNPIDTool> ( m_toolname , alg ) ;
      if ( nullptr  != pid ) { m_tool = pid ; }
    }
    //
  }
  //
  if ( !m_tool )
  {
    // try to use  Tool Service 
    SmartIF<IToolSvc> tsvc ( svc ) ;
    if ( tsvc ) 
    {
      const ANNGlobalPID::IChargedProtoANNPIDTool* pid = nullptr ;
      StatusCode sc = tsvc->retrieveTool ( m_toolname , pid ) ;
      if ( sc.isSuccess() && 0 != pid ) { m_tool = pid ; }
    }
  }
  //
  Assert ( m_tool , "Can't retrieve tool '" + m_toolname + "'" ) ;
}
// ============================================================================
// MANDATORY: the only one important method 
// ============================================================================
LoKi::Particles::ANNPID::result_type 
LoKi::Particles::ANNPID::operator() 
  ( LoKi::Particles::ANNPID::argument p ) const 
{
  //
  if ( !p ) 
  {
    Error("Invalid LHCb::Particle*, return -10000") ;
    return -10000 ;
  }
  //
  const LHCb::ProtoParticle* pp = p->proto() ;
  if ( !p ) 
  {
    Error("Invalid LHCb::ProtoParticle*, return -9999") ;
    return -9999 ;
  }
  //
  // use the tool! 
  ANNGlobalPID::IChargedProtoANNPIDTool::RetType res = 
    m_tool -> annPID ( pp , m_pid , m_tune );
  //
  if ( !res.status ) 
  {
    Error("Invalid result from IChargedProtoANNPIDTool, return -9998") ;
    return -9998 ;
  }
  return res.value ;  
}
// ============================================================================
// OPTIONAL: nice printout 
// ============================================================================
std::ostream& LoKi::Particles::ANNPID::fillStream 
( std::ostream& s ) const 
{
  s << "ANNPID('" << m_type << "','" << m_tune << "'";
  if ( m_toolname != s_toolname ) { s << ",'" << s_toolname << "'" ;}
  return  s << ")" ;
}
// ============================================================================
// The END 
// ============================================================================

