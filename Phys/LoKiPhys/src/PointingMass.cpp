#include "LoKi/PointingMass.h"
#include "LoKi/Kinematics.h"

namespace
{
	// the invalid 3-momentum & vertex position
	const LoKi::ThreeVector s_VECTOR = LoKi::ThreeVector ( 0, 0, -1 * Gaudi::Units::TeV );
	const LoKi::Point3D s_POINT = LoKi::Point3D ( 0, 0, -1 * Gaudi::Units::km );
}

// ============================================================================
// constructor from child selectors, including the
// "extra" child selector
// ============================================================================
LoKi::Particles::PointingMass::PointingMass(const LoKi::Child::Selector& mother,
		const LoKi::Child::Selector& tag,
		const LoKi::Child::Selector& probe,
		const LoKi::Child::Selector& extra) :
		LoKi::AuxFunBase ( std::tie ( mother, tag, probe, extra ) ) ,
		LoKi::BasicFunctors<const LHCb::Particle*>::Function (),
		LoKi::Vertices::VertexHolder ( ::s_POINT ),
		m_mother ( mother ), m_tag ( tag ), m_probe ( probe ), m_extra(extra)
{
	Assert ( m_mother.valid (), "The first  tree is invalid!" );
	Assert ( m_tag.valid (), "The tag tree is invalid!" );
	Assert ( m_probe.valid (), "The probe tree is invalid!" );
	Assert ( (*m_extra).valid (), "The extra tree is invalid!" );
}

LoKi::Particles::PointingMass::PointingMass(const LoKi::Child::Selector& mother,
		const LoKi::Child::Selector& tag,
		const LoKi::Child::Selector& probe) :
		LoKi::AuxFunBase ( std::tie ( mother, tag, probe ) ) ,
		LoKi::BasicFunctors<const LHCb::Particle*>::Function (),
		LoKi::Vertices::VertexHolder ( ::s_POINT ),
		m_mother ( mother ), m_tag ( tag ), m_probe ( probe )
{
	Assert ( m_mother.valid (), "The mother  tree is invalid!" );
	Assert ( m_tag.valid (), "The tag tree is invalid!" );
	Assert ( m_probe.valid (), "The probe tree is invalid!" );
}
// ============================================================================
// MANDATORY: virtual destructor
// ============================================================================
LoKi::Particles::PointingMass::~PointingMass()
{
}

// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::PointingMass*
LoKi::Particles::PointingMass::clone() const
{
	return new LoKi::Particles::PointingMass ( *this );
}
// ============================================================================

// ============================================================================
// get the proper decay components
// ==========================================================================
boost::optional<const LoKi::Particles::PointingMass::DecayStructure> LoKi::Particles::PointingMass::getComponents(const LHCb::Particle* p) const
{
	// The to-be-filled tuple of particles
	LoKi::Particles::PointingMass::DecayStructure returnSet;

	if (0 == p)
	{
		Error ( "LHCb::Particle* points to NULL." );

		return boost::optional<const LoKi::Particles::PointingMass::DecayStructure>{};
	}

	const LHCb::Particle* c1 = m_tag.child ( p );
	if (0 == c1)
	{
		Error ( "Invalid tag: '" + m_tag.printOut () + "'" );

		return boost::optional<const LoKi::Particles::PointingMass::DecayStructure>{};
	}

	returnSet.tag = c1;

	const LHCb::Particle* c2 = m_probe.child ( p );
	if (0 == c2)
	{
		Error ( "Invalid probe: '" + m_probe.printOut () + "'" );

		return boost::optional<const LoKi::Particles::PointingMass::DecayStructure>{};
	}

	returnSet.probe = c2;

	const LHCb::Particle* c3 = m_mother.child ( p );
	if (0 == c3)
	{
		Error ( "Invalid mother: '" + m_mother.printOut () + "'" );

		return boost::optional<const LoKi::Particles::PointingMass::DecayStructure>{};
	}
	returnSet.mother = c3;

	return returnSet;
}

std::ostream&
LoKi::Particles::PointingMass::fillStream ( std::ostream& s ) const
{ return s << "PointingMass functor called." ; }


// ==========================================================================
// MANDATORY: the only one essential method
// ==========================================================================
LoKi::Particles::PointingMass::result_type LoKi::Particles::PointingMass::operator()
		(LoKi::Particles::PointingMass::argument p) const
{
	if (0 == p)
	{
		Error ( "PointingMass: LHCb::Particle* points to NULL" );

		return LoKi::Constants::InvalidMass;
	}

	// Retrieve the LHCb::Particle's involved in this decay chain
	// for the child selectors.
	const auto decayComponentsResult = getComponents ( p );

	if (!decayComponentsResult)
	{
		Warning ( "Unable to get proper decay components, return 'Invalid Mass'" );

		return LoKi::Constants::InvalidMass;
	}

	// shortcut
	const auto& decayComponents = (*decayComponentsResult);

	// get the decay vertex:
	const LHCb::VertexBase* vx = decayComponents.mother->endVertex ();
	if (0 == vx)
	{
		Error ( "EndVertex is invalid, return 'Invalid Mass'" );

		return LoKi::Constants::InvalidMass;
	}

	const LHCb::VertexBase* pv = bestVertex ( p );

	if (0 == pv)
	{
		Error ( "BestVertex is invalid, return 'Invalid Mass'" );

		return LoKi::Constants::InvalidMass;
	}

	const LoKi::ThreeVector dir = vx->position () - pv->position ();

	 // this one will be changed in calculations, hence the copy and not const.
	LoKi::ThreeVector probe_vector = decayComponents.probe->momentum ().Vect ();

	auto Prb_p_PERP = LoKi::Kinematics::transverseMomentumDir( decayComponents.probe->momentum () , dir ) ;
	auto tag_p_perp = LoKi::Kinematics::transverseMomentumDir( decayComponents.tag->momentum () , dir ) ;

	const auto probe_mass = decayComponents.probe->momentum ().M ();
	const auto scale = (tag_p_perp) / (Prb_p_PERP);

	probe_vector *= scale;

	// Construct a new "mother vector", starting from a scaled version of the probe
	// momentum 4-vector, and adding the tag vector (and optionally, the extra vector)
	LoKi::LorentzVector new_d_vector;
	const double new_energy = ::sqrt( probe_mass * probe_mass + probe_vector.Mag2() );
	new_d_vector.SetPxPyPzE( probe_vector.X(), probe_vector.Y(), probe_vector.Z(), new_energy );
	new_d_vector += decayComponents.tag->momentum ();

	if (m_extra && (*m_extra).valid())
	{
		const LHCb::Particle* extraParticle = (*m_extra).child ( p );
		if (0 != extraParticle)
		{
			new_d_vector += extraParticle->momentum();
		}
	}

	return new_d_vector.M ();
}

