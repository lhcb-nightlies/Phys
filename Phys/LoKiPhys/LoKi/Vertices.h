// ============================================================================
#ifndef LOKI_VERTICES_H 
#define LOKI_VERTICES_H 1
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#include "Event/RecVertex.h"
// ============================================================================
// LoKiPhys 
// ============================================================================
#include "LoKi/Vertices0.h"
#include "LoKi/Vertices1.h"
#include "LoKi/Vertices2.h"
#include "LoKi/Vertices3.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project - 
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas, 
 *  contributions and advices from G.Raven, J.van Tilburg, 
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-16 
 */
// ============================================================================
namespace LoKi 
{ 
  // ==========================================================================
  /** @namespace  LoKi::Vertices Vertices.h LoKi/Vertices.h
   *  
   *  Namespace with collection of "Particle" functions for LoKi
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-02-16
   */
  namespace  Vertices 
  { 
    // ========================================================================
    GAUDI_API 
    std::size_t hash   ( const LHCb::VertexBase* vertex ) ;
  } //                                          end of namespace LoKi::Vertices 
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END 
// ============================================================================
#endif // LOKI_VERTICES_H
// ============================================================================
