// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/System.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/HybridBase.h"
#include "LoKi/HybridLock.h"
#include "LoKi/IHybridTool.h"
#include "LoKi/IHybridFactory.h"
// ============================================================================
/** @file
 *  definition and implementation file for class LoKi::HybridTool
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2004-06-29
 */
namespace LoKi
{
  // ==========================================================================
  namespace Hybrid
  {
    // ========================================================================
    /** @class Tool
     *
     *  Concrete implementation of LoKi::IHybridTool interface
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2004-06-29
     */
    class Tool : public extends2< LoKi::Hybrid::Base   ,
                                  LoKi::IHybridFactory ,
                                  LoKi::IHybridTool    >
    {
      // ======================================================================
      // friend factory forn instantiation
      friend struct ToolFactory<LoKi::Hybrid::Tool> ;
      // ======================================================================
    public:
      // ======================================================================
      /// finalization   of the tool
      StatusCode finalize  () override;
      // ======================================================================
      /// the update handler
      void propHandler ( Property& p  )
      {
        //
        if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return ; }
        //
        debug() << "Property is updated: " << p << endmsg ;
        //
      }
      // ======================================================================
    public:
      // ======================================================================
      // predicates
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param cuts placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string& pycode  ,
        LoKi::Types::Cut&  cuts    ,
        const std::string& context ) override
      { return _get ( pycode , m_cuts  , cuts , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param cuts placeholder for the  result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string& pycode  ,
        LoKi::Types::VCut& cuts    ,
        const std::string& context ) override
      { return _get ( pycode , m_vcuts , cuts , context  ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param cuts placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string& pycode  ,
        LoKi::Types::ACut& cuts    ,
        const std::string& context ) override
      { return _get ( pycode , m_acuts , cuts , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param cuts placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&  pycode  ,
        LoKi::Types::PPCut& cuts    ,
        const std::string&  context ) override
      { return _get ( pycode , m_ppcuts , cuts , context ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // functions
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func placehoplder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string& pycode  ,
        LoKi::Types::Fun& func     ,
        const std::string& context ) override
      { return _get ( pycode , m_func  , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string& pycode  ,
        LoKi::Types::VFun& func    ,
        const std::string& context ) override
      { return _get ( pycode , m_vfunc , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string& pycode  ,
        LoKi::Types::AFun& func    ,
        const std::string& context ) override
      { return _get ( pycode , m_afunc , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&  pycode  ,
        LoKi::Types::PPFun& func    ,
        const std::string&  context ) override
      { return _get ( pycode , m_ppfunc , func , context ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // "maps"
      // ======================================================================
      /** "Factory": get the the object from python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string& pycode  ,
        LoKi::Types::Map&  func    ,
        const std::string& context ) override
      { return _get ( pycode , m_maps , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string& pycode  ,
        LoKi::Types::VMap& func    ,
        const std::string& context ) override
      { return _get ( pycode , m_vmaps , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&  pycode  ,
        LoKi::Types::PPMap& func    ,
        const std::string&  context ) override
      { return _get ( pycode , m_ppmaps , func , context ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // "pipes"
      // ======================================================================
      /** "Factory": get the the object from python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string& pycode  ,
        LoKi::Types::Pipe& func    ,
        const std::string& context ) override
      { return _get ( pycode , m_pipes , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&  pycode  ,
        LoKi::Types::VPipe& func    ,
        const std::string&  context ) override
      { return _get ( pycode , m_vpipes , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&   pycode  ,
        LoKi::Types::PPPipe& func    ,
        const std::string&   context ) override
      { return _get ( pycode , m_pppipes , func , context ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // "fun-vals"
      // ======================================================================
      /** "Factory": get the the object from python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&   pycode  ,
        LoKi::Types::FunVal& func    ,
        const std::string&   context ) override
      { return _get ( pycode , m_funvals , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&    pycode  ,
        LoKi::Types::VFunVal& func    ,
        const std::string&    context ) override
      { return _get ( pycode , m_vfunvals , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&     pycode  ,
        LoKi::Types::PPFunVal& func    ,
        const std::string&     context ) override
      { return _get ( pycode , m_ppfunvals , func , context ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // "cut-vals"
      // ======================================================================
      /** "Factory": get the the object from python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&   pycode  ,
        LoKi::Types::CutVal& func    ,
        const std::string&   context ) override
      { return _get ( pycode , m_cutvals , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&    pycode  ,
        LoKi::Types::VCutVal& func    ,
        const std::string&    context ) override
      { return _get ( pycode , m_vcutvals , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&     pycode  ,
        LoKi::Types::PPCutVal& func    ,
        const std::string&     context ) override
      { return _get ( pycode , m_ppcutvals , func , context ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // "sources"
      // ======================================================================
      /** "Factory": get the the object from python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&    pycode  ,
        LoKi::Types::Source&  func    ,
        const std::string&    context ) override
      { return _get ( pycode , m_sources , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&     pycode  ,
        LoKi::Types::VSource&  func    ,
        const std::string&     context ) override
      { return _get ( pycode , m_vsources , func , context ) ; }
      // ======================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&     pycode  ,
        LoKi::Types::PPSource& func    ,
        const std::string&     context ) override
      { return _get ( pycode , m_ppsources , func , context ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // predicates:
      // ======================================================================
      /// set the C++ predicate for LHCb::Particle
      void set ( const LoKi::Types::Cuts&    cut ) override
      { LoKi::Hybrid::Base::_set ( m_cuts  , cut ) ; }
      /// set the C++ predicate for LHCb::Vertex
      void set ( const LoKi::Types::VCuts&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_vcuts , cut ) ; }
      /// set the C++ predicate for array of particles
      void set ( const LoKi::Types::ACuts&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_acuts , cut ) ; }
      /// set the C++ predicate for LHCb::ProtoParticle
      void set ( const LoKi::Types::PPCuts&  cut ) override
      { LoKi::Hybrid::Base::_set ( m_ppcuts , cut ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // functions:
      // ======================================================================
      /// set the C++ function for LHCb::Particle
      void set ( const LoKi::Types::Func&    cut ) override
      { LoKi::Hybrid::Base::_set ( m_func  , cut ) ; }
      /// set the C++ function for LHCb::Vertex
      void set ( const LoKi::Types::VFunc&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_vfunc  , cut ) ; }
      /// set the C++ function for arary of particles
      void set ( const LoKi::Types::AFunc&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_afunc  , cut ) ; }
      /// set the C++ function for LHCb::ProtoParticle
      void set ( const LoKi::Types::PPFunc&  cut ) override
      { LoKi::Hybrid::Base::_set ( m_ppfunc  , cut ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // maps:
      // ======================================================================
      /// set the C++ "map" for LHCb::Particle
      void set ( const LoKi::Types::Maps&    cut ) override
      { LoKi::Hybrid::Base::_set ( m_maps  , cut ) ; }
      /// set the C++ "map" for LHCb::Vertex
      void set ( const LoKi::Types::VMaps&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_vmaps  , cut ) ; }
      /// set the C++ "map" for LHCb::ProtoParticle
      void set ( const LoKi::Types::PPMaps&  cut ) override
      { LoKi::Hybrid::Base::_set ( m_ppmaps  , cut ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // pipes:
      // ======================================================================
      /// set the C++ "pipe" for LHCb::Particle
      void set ( const LoKi::Types::Pipes&    cut ) override
      { LoKi::Hybrid::Base::_set ( m_pipes   , cut ) ; }
      /// set the C++ "pipe" for LHCb::Vertex
      void set ( const LoKi::Types::VPipes&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_vpipes  , cut ) ; }
      /// set the C++ "pipe" for LHCb::ProtoParticle
      void set ( const LoKi::Types::PPPipes&  cut ) override
      { LoKi::Hybrid::Base::_set ( m_pppipes  , cut ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // fun-vals:
      // ======================================================================
      /// set the C++ "fun-val" for LHCb::Particle
      void set ( const LoKi::Types::FunVals&    cut ) override
      { LoKi::Hybrid::Base::_set ( m_funvals   , cut ) ; }
      /// set the C++ "fun-val" for LHCb::Vertex
      void set ( const LoKi::Types::VFunVals&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_vfunvals  , cut ) ; }
      /// set the C++ "fun-val" for LHCb::ProtoParticle
      void set ( const LoKi::Types::PPFunVals&  cut ) override
      { LoKi::Hybrid::Base::_set ( m_ppfunvals  , cut ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // cut-vals:
      // ======================================================================
      /// set the C++ "cut-val" for LHCb::Particle
      void set ( const LoKi::Types::CutVals&    cut ) override
      { LoKi::Hybrid::Base::_set ( m_cutvals   , cut ) ; }
      /// set the C++ "cut-val" for LHCb::Vertex
      void set ( const LoKi::Types::VCutVals&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_vcutvals  , cut ) ; }
      /// set the C++ "cut-val" for LHCb::ProtoParticle
      void set ( const LoKi::Types::PPCutVals&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_ppcutvals  , cut ) ; }
      // ======================================================================
    public:
      // ======================================================================
      // sources
      // ======================================================================
      /// set the C++ "source" for LHCb::Particle
      void set ( const LoKi::Types::Sources&    cut ) override
      { LoKi::Hybrid::Base::_set ( m_sources   , cut ) ; }
      /// set the C++ "source" for LHCb::Vertex
      void set ( const LoKi::Types::VSources&   cut ) override
      { LoKi::Hybrid::Base::_set ( m_vsources  , cut ) ; }
      /// set the C++ "source" for LHCb::ProtoParticle
      void set ( const LoKi::Types::PPSources&  cut ) override
      { LoKi::Hybrid::Base::_set ( m_ppsources  , cut ) ; }
      // ======================================================================
    protected:
      // ======================================================================
      /// constrcutor
      Tool
      ( const std::string& type   ,
        const std::string& name   ,
        const IInterface*  parent ) ;
      // ======================================================================
    private:
      // ======================================================================
      /// the copy constructor is disabled
      Tool           ( const Tool& )  ;     // the copy constructor is disabled
      /// the assignement operator  is disabled
      Tool& operator=( const Tool& )  ;                       // no assignement
      // ======================================================================
    private:
      // ======================================================================
      /// helper method to save many lines:
      template <class TYPE1,class TYPE2>
      inline StatusCode _get
      ( const std::string&                                            pycode  ,
        std::unique_ptr<LoKi::Functor<TYPE1,TYPE2>>&                  local   ,
        typename LoKi::Assignable<LoKi::Functor<TYPE1,TYPE2> >::Type& output  ,
        const std::string&                                            context ) ;
      // ======================================================================
    protected:
      // ======================================================================
      // local holder of cuts
      // predicates:
      std::unique_ptr<LoKi::Types::Cuts>       m_cuts       ;
      std::unique_ptr<LoKi::Types::VCuts>      m_vcuts      ;
      std::unique_ptr<LoKi::Types::ACuts>      m_acuts      ;
      std::unique_ptr<LoKi::Types::PPCuts>     m_ppcuts     ;
      // functions:
      std::unique_ptr<LoKi::Types::Func>       m_func       ;
      std::unique_ptr<LoKi::Types::VFunc>      m_vfunc      ;
      std::unique_ptr<LoKi::Types::AFunc>      m_afunc      ;
      std::unique_ptr<LoKi::Types::PPFunc>     m_ppfunc     ;
      // maps:
      std::unique_ptr<LoKi::Types::Maps>       m_maps       ;
      std::unique_ptr<LoKi::Types::VMaps>      m_vmaps      ;
      std::unique_ptr<LoKi::Types::PPMaps>     m_ppmaps     ;
      // pipes:
      std::unique_ptr<LoKi::Types::Pipes>      m_pipes      ;
      std::unique_ptr<LoKi::Types::VPipes>     m_vpipes     ;
      std::unique_ptr<LoKi::Types::PPPipes>    m_pppipes    ;
      // fun-vals:
      std::unique_ptr<LoKi::Types::FunVals>    m_funvals    ;
      std::unique_ptr<LoKi::Types::VFunVals>   m_vfunvals   ;
      std::unique_ptr<LoKi::Types::PPFunVals>  m_ppfunvals  ;
      // cut-vals:
      std::unique_ptr<LoKi::Types::CutVals>    m_cutvals    ;
      std::unique_ptr<LoKi::Types::VCutVals>   m_vcutvals   ;
      std::unique_ptr<LoKi::Types::PPCutVals>  m_ppcutvals  ;
      // sources:
      std::unique_ptr<LoKi::Types::Sources>    m_sources    ;
      std::unique_ptr<LoKi::Types::VSources>   m_vsources   ;
      std::unique_ptr<LoKi::Types::PPSources>  m_ppsources  ;
      //
      typedef std::vector<std::string> Modules ;
      Modules     m_modules ;
      std::string m_actor   = "LoKi.Hybrid.Engine()";
      typedef std::vector<std::string> Lines   ;
      Lines       m_lines   ;
      // ======================================================================
    } ;
    // ========================================================================
  } //                                            end of namespace LoKi::Hybrid
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// helper method to sdave many lines:
// ============================================================================
template <class TYPE1,class TYPE2>
inline StatusCode LoKi::Hybrid::Tool::_get
( const std::string&                                            pycode  ,
  std::unique_ptr<LoKi::Functor<TYPE1,TYPE2>>&                  local   ,
  typename LoKi::Assignable<LoKi::Functor<TYPE1,TYPE2> >::Type& output  ,
  const std::string&                                            context )
{
  // prepare the actual python code
  std::string code = makeCode
    ( m_modules , m_actor , pycode , m_lines , context ) ;
  // define the scope:
  LoKi::Hybrid::Lock lock ( this ) ;   ///< ATTENTION: the scope is locked!!
  //
  // use the base class method
  StatusCode sc = LoKi::Hybrid::Base::_get_ ( code , local , output ) ;
  if ( sc.isFailure() )
  { return Error ( "Invalid object for the code '" + pycode + "'"    ) ; } // RETURN
  //
  return StatusCode::SUCCESS ;
}
// ============================================================================
// Standard constructor
// ============================================================================
LoKi::Hybrid::Tool::Tool
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent )
  : base_class ( type , name , parent )
{
  //
  m_modules.push_back ( "LoKiPhys.decorators"           ) ;
  m_modules.push_back ( "LoKiProtoParticles.decorators" ) ;
  m_modules.push_back ( "LoKiArrayFunctors.decorators"  ) ;
  m_modules.push_back ( "LoKiCore.functions"            ) ;
  m_modules.push_back ( "LoKiCore.math"                 ) ;
  //
  declareProperty
    ( "Modules" ,
      m_modules ,
      "Python modules to be imported" )
    -> declareUpdateHandler
    ( &LoKi::Hybrid::Tool::propHandler , this ) ;
  declareProperty
    ( "Actor"   ,
      m_actor   ,
      "The processing engine"         )
    -> declareUpdateHandler
    ( &LoKi::Hybrid::Tool::propHandler , this ) ;
  declareProperty
    ( "Lines"   ,
      m_lines   ,
      "Additional python lines to be used in Bender/Pythoni script" )
    -> declareUpdateHandler
    ( &LoKi::Hybrid::Tool::propHandler , this ) ;
  //
  // ==========================================================================
  // C++
  // ==========================================================================
  m_cpplines.push_back ( "#include \"LoKi/LoKiTrack.h\""         ) ;
  m_cpplines.push_back ( "#include \"LoKi/LoKiProtoParticles.h\"" ) ;
  m_cpplines.push_back ( "#include \"LoKi/LoKiPhys.h\""           ) ;
  m_cpplines.push_back ( "#include \"LoKi/LoKiArrayFunctors.h\""  ) ;
  // ==========================================================================
}
// ============================================================================
// finalization of the tool
// ============================================================================
StatusCode LoKi::Hybrid::Tool::finalize  ()
{
  // predicates:
  m_cuts.reset();
  m_vcuts.reset();
  m_acuts.reset();
  m_ppcuts.reset();
  // functions:
  m_func.reset();
  m_vfunc.reset();
  m_afunc.reset();
  m_ppfunc.reset();
  // maps:
  m_maps.reset();
  m_vmaps.reset();
  m_ppmaps.reset();
  // pipes:
  m_pipes.reset();
  m_vpipes.reset();
  m_pppipes.reset();
  // fun-vals:
  m_funvals.reset();
  m_vfunvals.reset();
  m_ppfunvals.reset();
  // cut-vals:
  m_cutvals.reset();
  m_vcutvals.reset();
  m_ppcutvals.reset();
  // sources:
  m_sources.reset();
  m_vsources.reset();
  m_ppsources.reset();
  //
  // finalize the base
  return LoKi::Hybrid::Base::finalize() ;
}
// ============================================================================
DECLARE_NAMESPACE_TOOL_FACTORY(LoKi::Hybrid,Tool)
// ============================================================================
// The END
// ============================================================================
