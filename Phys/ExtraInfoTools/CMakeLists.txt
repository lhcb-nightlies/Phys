################################################################################
# Package: ExtraInfoTools
################################################################################
gaudi_subdir(ExtraInfoTools)

gaudi_depends_on_subdirs(Phys/DaVinciKernel)

find_package(ROOT)
find_package(Boost)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_module(ExtraInfoTools
                 src/*.cpp
                 LINK_LIBRARIES DaVinciKernelLib)
