// $Id: L0TriggerTisTos.cpp,v 1.1 2010-06-23 22:39:24 tskwarni Exp $
// Include files

#include "Event/HltDecReports.h"
#include "Event/HltSelReports.h"

// local
#include "L0TriggerTisTos.h"

//-----------------------------------------------------------------------------
// Implementation file for class : L0TriggerTisTos
//
// 2010-06-23 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( L0TriggerTisTos )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
L0TriggerTisTos::L0TriggerTisTos( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent )
  : TriggerTisTos ( type, name , parent )
{
  declareInterface<ITriggerTisTos>(this);
  setProperty("HltDecReportsLocation", "HltLikeL0/DecReports" );
  setProperty("HltSelReportsLocation", "HltLikeL0/SelReports" );
}

//=============================================================================
// Destructor
//=============================================================================
L0TriggerTisTos::~L0TriggerTisTos() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode L0TriggerTisTos::initialize() 
{
  const StatusCode sc = TriggerTisTos::initialize(); 
  if ( sc.isFailure() ) return sc; 
  if ((getTOSFrac( kMuon ) <= 0 ) || (getTOSFrac( kEcal ) <= 0) || (getTOSFrac( kHcal ) <= 0) ){
    Warning("L0 TOS fractions set zero or negative for Muons, ECAL or HCAL. This will make the matching with L0 objects useless.").ignore();
    Warning("TOS frac muon " + std::to_string(getTOSFrac( kMuon ))).ignore();
    Warning("TOS frac ECAL " + std::to_string(getTOSFrac( kEcal ))).ignore();
    Warning("TOS frac HCAL " + std::to_string(getTOSFrac( kHcal ))).ignore();
  }

  debug() << "L0DecReportsMaker and L0SelReportsMaker must be executed before using this tool" 
          << endmsg;  
  debug() << "DecReport location = " << m_HltDecReportsLocation.value()
          << " SelReport location = " << m_HltSelReportsLocation.value()
          << endmsg; 
  return sc;
}
