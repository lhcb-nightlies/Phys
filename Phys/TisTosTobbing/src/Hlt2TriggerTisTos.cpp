// $Id:  $
// Include files

#include "Event/HltDecReports.h"
#include "Event/HltSelReports.h"

// local
#include "Hlt2TriggerTisTos.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Hlt2TriggerTisTos
//
// 2010-06-23 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( Hlt2TriggerTisTos )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Hlt2TriggerTisTos::Hlt2TriggerTisTos( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent )
  : TriggerTisTos ( type, name , parent )
{
  declareInterface<ITriggerTisTos>(this);
  setProperty("HltDecReportsLocation", "Hlt2/DecReports" );
  setProperty("HltSelReportsLocation", "Hlt2/SelReports" );

  // For Run 2 data Hlt2SelReports contain muon hits when IsMuonLoose is fulfiled.
  // Due to an inconsistency with the TriggerTisTosTool which asks for IsMuon to take
  // offline hits into account, this leads to particles being TPS instead of TOS.
  // The following line disables that Muon hits are considered for TOS decisions.
  // For a more detailed disucssion look at https://its.cern.ch/jira/browse/LHCBPS-1746 .
  setProperty("TOSFracMuon", 0.0 );
  
}

//=============================================================================
// Destructor
//=============================================================================
Hlt2TriggerTisTos::~Hlt2TriggerTisTos() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode Hlt2TriggerTisTos::initialize() 
{
  const StatusCode sc = TriggerTisTos::initialize(); 
  if ( sc.isFailure() ) return sc; 

  if ((getTOSFrac( kMuon ) > 0 )){
    Warning("L0 TOS fractions set positive for Muons. Be aware that for Run 2 data there is an issue when"
	    "including Muons in the Hlt2 TISTOS decision. See https://its.cern.ch/jira/browse/LHCBPS-1746.").ignore();
    Warning("TOS frac muon " + std::to_string(getTOSFrac( kMuon ))).ignore();
  }

  return sc;
}
