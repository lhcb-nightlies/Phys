#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file PhysConf/Selections.py
#  Collection of high-level blocks for ``Seelctions''-framework
#
#  Basic blocks: 
# - AutomaticData              : selection that gets data from TES (or Datra-On-Demand)
# - MergedSelection            : merge several selections into one ('OR'-mode)
# - SelectionSequence          : create the selection sequence 
# - MultiSelectionSequence     : create the sequence for several selections 
#
#  Derived blocks: 
# - SimpleSelection            : create 1-step selection 
# - FilterSelection            : selection with FilterDesktop    algorithm 
# - CombineSelection           : selection with CombineParticles algorithm 
# - Combine3BodySelection      : selection with DaVinci::N3BodyDecays algorithm 
# - Combine4BodySelection      : selection with DaVinci::N4BodyDecays algorithm
# - ValidBPVSelection          : check valid associated best primary vertex 
# - TupleSelection             : selection with DecayTreeTuple algorithm 
# - PrintSelection             : helper selection for debugging purposes
# - LimitSelection             : limit selection 
# - CheckPVSelection           : embed CheckPV algorithm  into selection sequence 
# - MomentumScaling            : insert momentum scaling  into selection sequence
# - MomentumSmear              : insert momentum smearing into selection sequence
# - RebuildSelection           : re-run/re-build some existing common/standard selections
# - TriggerSelection           : "event" selection to require certain trigger decisions
# - L0Selection                : specialization of TriggerSelection for L0-trigger 
# - Hlt1Selection              : specialization of TriggerSelection for Hlt1-trigger 
# - Hlt2Selection              : specialization of TriggerSelection for Hlt2-trigger 
# - StrippingSelection         : specialization of TriggerSelection for Stripping 
# - TisTosSelection            : specialization of FilterSelection for Tis/Tos/Tus/Tps-selections
# - L0TOSSelection             : specialization of TisTosSelection for L0-TOS 
# - L0TISSelection             : specialization of TisTosSelection for L0-TIS 
# - Hlt1TOSSelection           : specialization of TisTosSelection for Hlt1-TOS 
# - Hlt1TISSelection           : specialization of TisTosSelection for Hlt1-TIS 
# - Hlt2TOSSelection           : specialization of TisTosSelection for Hlt2-TOS 
# - Hlt2TISSelection           : specialization of TisTosSelection for Hlt2-TIS
#
#  @author Juan  PALACIOS 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#
# =============================================================================
""" Collection of high-level blocks for ``Selections''-framework:

Basic blocks: 
- AutomaticData              :  selection that gets data from TES (or Data-On-Demand)
- MergedSelection            :  merge several selections into one ('OR'-mode)
- SelectionSequence          :  create the selection sequence 
- MultiSelectionSequence     :  create the sequence for several selections 

Derived blocks: 
- SimpleSelection            : create 1-step selection 
- FilterSelection            : selection with FilterDesktop    algorithm 
- CombineSelection           : selection with CombineParticles algorithm 
- Combine3BodySelection      : selection with DaVinci::N3BodyDecays algorithm 
- Combine4BodySelection      : selection with DaVinci::N4BodyDecays algorithm 
- TupleSelection             : selection with DecayTreeTuple algorithm 
- ValidBPVSelection          : Specialization of FilterSelection to check valid associated best primary vertex 
- PrintSelection             : helper selection for debugging purposes
- LimitSelection             : limit selection 
- CheckPVSelection           : embed CheckPV algorithm into selectiton sequence 
- TriggerSelection           : variant of EventSelection that allows select certain trigger decisions
- L0Selection                : variant of TriggerSelection that allows select certain L0   trigger decisions
- Hlt1Selection              : variant of TriggerSelection that allows select certain Hlt1 trigger decisions
- Hlt2Selection              : variant of TriggerSelection that allows select certain Hlt2 trigger decisions
- StrippingSelection         : variant of TriggerSelection that allows select certain stripping    decisions
- MomentumScaling            : insert momentum scaling  into selection sequence 
- MomentumSmear              : insert momentum smearing into selection sequence 
- PayloadSelection           : pseudo-selection that insert certain algoritm into data flow
- CheckPVSelection           : embed CheckPV algorithm  into selection sequence 
- RebuildSelection           : re-run/re-build some existing common/standard selections
- TisTosSelection            : specialization of FilterSelection for Tis/Tos/Tus/Tps-selections
- L0TOSSelection             : specialization of TisTosSelection for L0-TOS 
- L0TISSelection             : specialization of TisTosSelection for L0-TIS 
- Hlt1TOSSelection           : specialization of TisTosSelection for Hlt1-TOS 
- Hlt1TISSelection           : specialization of TisTosSelection for Hlt1-TIS 
- Hlt2TOSSelection           : specialization of TisTosSelection for Hlt2-TOS 
- Hlt2TISSelection           : specialization of TisTosSelection for Hlt2-TIS 
"""
# =============================================================================
__author__  = 'Juan PALACIOS, Vanya BELYAEV'
__version__ = '$Revision$'
__date__    = '2016-03-12'
__all__     = ( 'AutomaticData'                 ,
                'Selection'                     ,
                'MergedSelection'               ,
                'SelectionSequence'             ,
                'MultiSelectionSequence'        ,
                #
                'SimpleSelection'               , 
                'FilterSelection'               , 
                'PrintSelection'                , 
                'LimitSelection'                ,
                'CombineSelection'              ,
                'Combine3BodySelection'         ,
                'Combine4BodySelection'         ,
                'TupleSelection'                ,
                'ValidBPVSelection'             , 
                ##
                'CheckPVSelection'              ,
                ##
                'TriggerSelection'              ,
                'L0Selection'                   ,
                'Hlt1Selection'                 ,
                'Hlt2Selection'                 ,
                'StrippingSelection'            ,               
                ##
                'RebuildSelection'              , 
                ##
                'PayloadSelection'              ,  
                'MomentumSmear'                 , 
                'MomentumScaling'               , ## apply momentum scaling calibration 
                #
                'TisTosSelection'               ,
                'L0TOSSelection'                ,
                'L0TISSelection'                ,
                'Hlt1TOSSelection'              ,
                'Hlt1TISSelection'              ,
                'Hlt2TOSSelection'              ,
                'Hlt2TISSelection'              ,
                )
# =============================================================================
## import all blocks from wrappers 
from PhysSelPython.Selections import *
# =============================================================================


# =============================================================================
if '__main__' == __name__ :

    print   80*'*'  
    print  __doc__ 
    print  ' Author  : %s ' %  __author__  
    print  ' Version : %s ' %  __version__  
    print  ' Date    : %s ' %  __date__   
    print  ' Symbols : %s ' %  list ( __all__ )
    print   80*'*'
    
    from sys import modules
    _this = modules[ __name__ ]
    for o in __all__ :
        obj = getattr ( _this , o , None )
        if obj and obj.__doc__ :
            doc = obj.__doc__.replace('\n', '\n#') 
            print "# %s\n# %s" % ( o , doc ) 
    print   80*'*'  
 
# =============================================================================
# The END 
# =============================================================================
