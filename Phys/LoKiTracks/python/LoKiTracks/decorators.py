#!/usr/bin/env python
# =============================================================================
## @file
#  The set of basic objects from LoKiTrack library
#
#        This file is a part of LoKi project - 
#    "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas, 
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @author Vanya BELYAEV ibelyaev@itep.ru
#  @date 2007-06-09
# =============================================================================
"""
The set of basic objects from LoKiTrack library

      This file is a part of LoKi project - 
``C++ ToolKit  for Smart and Friendly Physics Analysis''

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas, 
contributions and advices from G.Raven, J.van Tilburg, 
A.Golutvin, P.Koppenburg have been used in the design.
"""
# =============================================================================
__author__  = "Vanya BELYAEV  Ivan.Belyaev@itep.ru "
__date__    = "2010-07-17"
__version__ = "CVS tag $Name:$, version $Revision$ "
# =============================================================================

from LoKiTracks.functions import *
from LoKiTrack.decorators import _decorate


_decorated = _decorate( __name__ )


from LoKiTrack.decorators import *

# =============================================================================
if '__main__' == __name__ :
    
    print '*'*120
    print                      __doc__
    print ' Author  : %s ' %   __author__    
    print ' Version : %s ' %   __version__
    print ' Date    : %s ' %   __date__
    print ' Number of properly decorated types: %s'%len(_decorated)
    print '*'*120
    

# =============================================================================
# The END 
# =============================================================================
