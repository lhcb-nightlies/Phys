#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
"""Wrapper classes for a DaVinci offline physics selection. The following classes
are available:

- Selection             Wraps a selection configurable and the selections it requires
- DataOnDemand          Wraps a string TES location to make it look like a Selection
- AutomaticData         Wraps a string TES location to make it look like a Selection
- SelectionSequence     Creates a sequence from a selection such that all the
                          sub-selections required are executed in the right order
- MergedSelection       Merges a set of selections in OR mode, writing all data to a single location.

- EventSelection        Wraps an algorithm that selects an event and produces no output data.
- VoidEventSelection    Selects event based on TES container properties, applying a LoKi__VoidFilter compatible cut to the data from a required selection.

- TriggerSelection      Variant of EventSelection that allows select certain trigger decisions
- L0Selection           Variant of TriggerSelection that allows select certain L0   trigger decisions
- Hlt1Selection         Variant of TriggerSelection that allows select certain Hlt1 trigger decisions
- Hlt2Selection         Variant of TriggerSelection that allows select certain Hlt2 trigger decisions
- StrippingSelection    Variant of TriggerSelection that allows select certain stripping    decisions

- PassThroughSelection
- PrintSelection        Specialization of PassThroughSelection to print the objects
- CheckPVSelection      Specialization of PassThroughSelection to limit the selection to use CheckPV 
- LimitSelection        Specialization of PassThroughSelection to limit the selection 

- SimpleSelection       Simple compact 1-step way to create the selection 
- CombineSelection      Specialization of SimpleSelection for CombineParticles algorithm
- Combine3BodySelection Specialization of SimpleSelection for DaVinci::N3BodyDecays algorithm
- Combine4BodySelection Specialization of SimpleSelection for DaVinci::N4BodyDecays algorithm
- TupleSelection        Specialization of SimpleSelection for DecayTreeTuple algorithm
- FilterSelection       Specialization of SimpleSelection for filtering using FilterDesktop

- ValidBPVSelection     Specialization of FilterSelection to check valid associated best primary vertex 
- TisTosSelection       Specialization of FilterSelection for Tis/Tos/Tus/Tps-selections
- L0TOSSelection        Specialization of TisTosSelection for L0-TOS 
- L0TISSelection        Specialization of TisTosSelection for L0-TIS 
- Hlt1TOSSelection      Specialization of TisTosSelection for Hlt1-TOS 
- Hlt1TISSelection      Specialization of TisTosSelection for Hlt1-TIS 
- Hlt2TOSSelection      Specialization of TisTosSelection for Hlt2-TOS 
- Hlt2TISSelection      Specialization of TisTosSelection for Hlt2-TIS 

- PayloadSelection      Pseudo-selection that insert certain algorithm into data flow
- MomentumSmear         Pseudo-selection that applies (globally) momentum smearing via TrackSmearScale algorithm
- RebuildSelection      Re-run/re-build some existing common/standard selections

"""
# =============================================================================
__author__ = "Juan PALACIOS juan.palacios@nikhef.nl"
__all__    = ( 'DataOnDemand'                  ,
               'AutomaticData'                 ,
               'Selection'                     ,
               'MergedSelection'               ,
               'EventSelection'                ,
               'PassThroughSelection'          ,
               'ChargedProtoParticleSelection' ,
               'SelectionSequence'             ,
               'MultiSelectionSequence'        ,
               'SelSequence'                   ,
               'NameError'                     ,
               'NonEmptyInputLocations'        ,
               'IncompatibleInputLocations'    ,
               ##
               'SimpleSelection'               , 
               'FilterSelection'               , 
               'PrintSelection'                , 
               'LimitSelection'                ,
               'CombineSelection'              ,
               'Combine3BodySelection'         ,
               'Combine4BodySelection'         ,
               'TupleSelection'                ,
               'ValidBPVSelection'             , ## check valid associated best primary vertex 
               ##
               'CheckPVSelection'              ,
               ##
               'TriggerSelection'              ,
               'L0Selection'                   ,
               'Hlt1Selection'                 ,
               'Hlt2Selection'                 ,
               'StrippingSelection'            ,               
               ##
               'RebuildSelection'              ,             
               ## 
               'PayloadSelection'              ,
               'MomentumSmear'                 ,
               ## TIS&TOS
               'TisTosSelection'  ,
               #
               'L0TOSSelection'   ,
               'L0TISSelection'   ,
               'Hlt1TOSSelection' ,
               'Hlt1TISSelection' ,
               'Hlt2TOSSelection' ,
               'Hlt2TISSelection' ,
               )

from copy import copy

from SelPy.selection import ( flatAlgorithmList,
                              NamedObject,
                              UniquelyNamedObject,
                              ClonableObject,
                              SelectionBase,
                              AutomaticData,
                              NameError,
                              NonEmptyInputLocations,
                              IncompatibleInputLocations)

from   SelPy.utils     import update_dict_overlap
from   SelPy.utils     import CloneCallable
from   SelPy.utils     import connectToRequiredSelections
from   SelPy.utils     import makeOutputLocation

from   SelPy.selection import Selection            as Sel
from   SelPy.selection import SelectionSequence    as SelSequence
from   SelPy.selection import EventSelection       as EvtSel
from   SelPy.selection import PassThroughSelection as PassThroughSel
from   SelPy.selection import AutomaticData        as autodata

from   GaudiKernel.Configurable import Configurable
from   GaudiConfUtils           import configurableExists, isConfigurable
from   GaudiConfUtils           import ConfigurableGenerators
import Configurables
from   Gaudi.Configuration      import log

def checkName(name) :
    if configurableExists(name) :
        raise NameError('Could not instantiate Selection '+name+' because Configurable with the same name already exists. Try a different name for your Selection')

def checkConfigurable(obj) :
    if type(obj).__name__ == obj.name().replace('::','__') :
        raise NameError('Could not instantiate Selection because input Configurable '+obj.name()+' has default name. This is too unsafe to be allowed.')

# =============================================================================
## Construct a selection of type selType with construction arguments.
#  Checks if name already belongs to an existing Configurable.
#  Checks whether input is Configurable and if so, extracts its non-default
#  parameters and passes them as a configurableGenerator.
def selectionWrapper(selType, name, *args, **kwargs) :
    """Construct a selection of type selType with construction arguments.
    Checks if name already belongs to an existing Configurable.
    Checks whether input is Configurable and if so, extracts its non-default
    parameters and passes them as a configurableGenerator.
    """
    checkName(name)

    algorithm = kwargs.pop('Algorithm')
    if isConfigurable( algorithm )  :
        checkConfigurable( algorithm)
        algGen=CloneCallable(algorithm)
        kwargs['ConfGenerator'] = algGen
    else :
        kwargs['ConfGenerator'] = algorithm
    return selType(name, *args, **kwargs)

# =============================================================================
## Wrapper around SelPy.Selection. Since SelPy.Selection takes a
#  ConfigurableGenerator as input and constructs a Configurable 
#  with it it's own name, check first whether a configurable
#  with that name already exists and raise a NameError if that is the case.
#  If not, construct and return a SelPy.selection.Selection.
def Selection(name, *args, **kwargs) :
    """Wrapper around SelPy.Selection. Since SelPy.Selection takes a
    ConfigurableGenerator as input and constructs a Configurable 
    with it it's own name, check first whether a configurable
    with that name already exists and raise a NameError if that is the case.
    If not, construct and return a SelPy.selection.Selection.
    """
    return selectionWrapper(Sel, name, *args, **kwargs)

Selection.__doc__ += '\nSelPy.selection.Selection:\n'+Sel.__doc__


# =============================================================================
## Wrapper around SelPy.EventSelection. Since SelPy.EventSelection takes a
#  ConfigurableGenerator as input and constructs a Configurable 
#  with it it's own name, check first whether a configurable
#  with that name already exists and raise a NameError if that is the case.
#  If not, construct and return a SelPy.selection.EventSelection.
def EventSelection(name, *args, **kwargs) :
    """Wrapper around SelPy.EventSelection. Since SelPy.EventSelection takes a
    ConfigurableGenerator as input and constructs a Configurable 
    with it it's own name, check first whether a configurable
    with that name already exists and raise a NameError if that is the case.
    If not, construct and return a SelPy.selection.EventSelection.
    """
    return selectionWrapper(EvtSel, name, *args, **kwargs)

EventSelection.__doc__ += '\nSelPy.selection.EventSelection:\n'+EvtSel.__doc__

                     
# =============================================================================
## Wrapper around SelPy.PassThroughSelection. Since
#  SelPy.PassThroughSelection takes a
#  ConfigurableGenerator as input and constructs a Configurable 
#  with it it's own name, check first whether a configurable
#  with that name already exists and raise a NameError if that is the case.
#  If not, construct and return a SelPy.selection.PassThroughSelection.
def PassThroughSelection(name, *args, **kwargs) :
    """Wrapper around SelPy.PassThroughSelection. Since
    SelPy.PassThroughSelection takes a
    ConfigurableGenerator as input and constructs a Configurable 
    with it it's own name, check first whether a configurable
    with that name already exists and raise a NameError if that is the case.
    If not, construct and return a SelPy.selection.PassThroughSelection.
    """
    return selectionWrapper(PassThroughSel, name, *args, **kwargs)    
PassThroughSelection.__doc__ += '\nSelPy.selection.PassThroughSelection:\n'+PassThroughSel.__doc__

# =============================================================================
## Simple wrapper for a data location. To be used for locations
#  that are guaranteed to be populated. This could be a location
#  on a DST or a location registered to the DataOnDemandSvc.
#  Returns output location via outputLocation() method.
#  Can be used as a Selection in RequiredSelections field of other
#  Selections.
# 
#  Example: wrap StdLoosePions
#  @code 
#  >>> SelStdLoosePions = AutomaticData(Location = 'Phys/StdLoosePions')
#  >>> SelStdLoosePions.outputLocation()
#  ... 'Phys/StdLoosePions'
#  >>> SelStdLoosePions.name()
#  ... 'Phys_StdLoosePions'
#  >>> pions = AutomaticData(Location='Phys/StdLoosePions/Particles')
#  >>> pions.outputLocation()
#  ... 'Phys/StdLoosePions/Particles'
#  @endcode 
class AutomaticData(NamedObject, SelectionBase) :
    """Simple wrapper for a data location. To be used for locations
    that are guaranteed to be populated. This could be a location
    on a DST or a location registered to the DataOnDemandSvc.
    Returns output location via outputLocation() method.
    Can be used as a Selection in RequiredSelections field of other
    Selections.

    Example: wrap StdLoosePions

    >>> SelStdLoosePions = AutomaticData(Location = 'Phys/StdLoosePions')
    >>> SelStdLoosePions.outputLocation()
    'Phys/StdLoosePions'
    >>> SelStdLoosePions.name()
    'Phys_StdLoosePions'
    >>> pions = AutomaticData(Location='Phys/StdLoosePions/Particles')
    >>> pions.outputLocation()
    'Phys/StdLoosePions/Particles'
    """
    
    def __init__( self                 ,
                  Location             ,
                  UseRootInTES = True  ,
                  monitor      = False ) :
        
        NamedObject.__init__(self, Location.replace('/', '_'))
        
        from Configurables import LoKi__VoidFilter as VoidFilter

        _alg = VoidFilter( 'SelFilter'+self.name() )
        
        if not monitor : 
            _alg.Code = """
            0<CONTAINS('%s',%s)
            """ % ( Location , UseRootInTES )
        else :
            _alg.Preambulo += [
                "from LoKiCore.functions import monitor"     ,
                "from LoKiCore.math      import max as lmax" ,
                ]
            ## create monitored functor 
            _alg.Code       = """
            0< monitor ( lmax ( CONTAINS('%s',%s), FZERO ) ,'# %s', 0 )
            """ % ( Location , UseRootInTES , Location )
            
        SelectionBase.__init__(self,
                               algorithm = _alg,
                               outputLocation=Location,
                               requiredSelections = [] )

DataOnDemand = AutomaticData


# =============================================================================
## Selection class for event selection based on contents of TES location.
#  Can be used just like a Selection object.
#  Constructor argument Code is LoKi__VoidFilter compatible single-location
#  cut, where the location is expressed as '<Location>' and determined
#  from the RequiredSelection's outputLocation(). The outputLocation() of
#  this algorithm is the same as that of the RequiredSelection.
#  Example:
#  @code 
#  >>> from PhysSelPython.Wrappers import VoidEventSelection, SelectionSequence
#  >>> evtSel = VoidEventSelection( name='MyEvtSel',
#  ... Code="CONTAINS('<Location>')>0",
#  ... RequiredSelection = AutomaticData(....) )
#  >>> help(SelectionSequence)
#  >>> selSeq = SelectionSequence('MyEvtSelSeq', TopSelection = evtSel)
#  @endcode 
class VoidEventSelection(UniquelyNamedObject,
                         ClonableObject,
                         SelectionBase) :
    """Selection class for event selection based on contents of TES location.
    Can be used just like a Selection object.
    Constructor argument Code is LoKi__VoidFilter compatible single-location
    cut, where the location is expressed as '<Location>' and determined
    from the RequiredSelection's outputLocation(). The outputLocation() of
    this algorithm is the same as that of the RequiredSelection.
    Example:
    from PhysSelPython.Wrappers import VoidEventSelection, SelectionSequence
    evtSel = VoidEventSelection( name='MyEvtSel',
                                 Code="CONTAINS('<Location>')>0",
                                 RequiredSelection = AutomaticData(....) )
    help(SelectionSequence)
    selSeq = SelectionSequence('MyEvtSelSeq', TopSelection = evtSel)
    """

    __author__ = "Juan Palacios palacios@physik.uzh.ch"
    
    def __init__(self,
                 name,
                 Code,
                 RequiredSelection ) :

        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        checkName(self.name())
        
        _code = Code.replace('<Location>',
                             "'"+RequiredSelection.outputLocation()+"'")
        _code = _code.replace('\"\'', '\'').replace('\'\"', '\'').replace('\'\'','\'')
        
        from Configurables import LoKi__VoidFilter as VoidFilter
        _alg = VoidFilter(self.name(), Code = _code)

        SelectionBase.__init__(self,
                               algorithm = _alg,
                               outputLocation = RequiredSelection.outputLocation(),
                               requiredSelections = [RequiredSelection])

    
# =============================================================================
## Selection wrapper class for merging output of various Selections.
#  Can be used just like any Selection object.
#  Example:
#  @code     
#  >>> from PhysSelPython.Wrappers import MergedSelection, AutomaticData, SelectionSequence
#  >>> sel00 = AutomaticData(Location = 'Phys/Sel00')
#  >>> sel01 = AutomaticData(Location = 'Phys/Sel01')
#  >>> sel02 = AutomaticData(Location = 'Phys/Sel02')
#  >>> sel03 = AutomaticData(Location = 'Phys/Sel03')
#  >>> merge = MergedSelection('SelMerger', RequiredSelections = [sel00, sel01, sel02, sel03])
#  >>> seqMerge = SelectionSequence('SeqMergeSelections, TopSelection = merge)
#  @endcode
#  This will OR all the selections and place the output in merge.outputLocation().
class MergedSelection(NamedObject    ,
                      ClonableObject ,
                      SelectionBase  ) :
    """Selection wrapper class for merging output of various Selections.
    Can be used just like any Selection object.
    Example:
    
    from PhysSelPython.Wrappers import MergedSelection, AutomaticData, SelectionSequence
    sel00 = AutomaticData(Location = 'Phys/Sel00')
    sel01 = AutomaticData(Location = 'Phys/Sel01')
    sel02 = AutomaticData(Location = 'Phys/Sel02')
    sel03 = AutomaticData(Location = 'Phys/Sel03')
    merge = MergedSelection('SelMerger', RequiredSelections = [sel00, sel01, sel02, sel03])
    seqMerge = SelectionSequence('SeqMergeSelections, TopSelection = merge)

    This will OR all the selections and place the output in merge.outputLocation().
    """
    def __init__(self                                              ,
                 name                                              ,
                 RequiredSelections = []                           ,
                 Code               = 'ALL'                        ,
                 Unique             = False                        ,
                 OutputBranch       = "Phys"                       ,
                 sequencerType      = Configurables.GaudiSequencer ) : 
        
        
        NamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())

        if Unique : algtype = ConfigurableGenerators.FilterUnique  ( Code = Code )
        else      : algtype = ConfigurableGenerators.FilterDesktop ( Code = Code )
        
        self._sel = Selection ( name                                    ,
                                Algorithm          = algtype            ,
                                RequiredSelections = RequiredSelections ,
                                OutputBranch       = OutputBranch       )
        
        self._selections = RequiredSelections 
        
        ## it was 
        #self._algos1 = flatAlgorithmList(self._sel)

        ## it now 
        self._algos = [] 
        for s in RequiredSelections :
            lst   = flatAlgorithmList ( s )
            if   not lst : continue 
            _seq  =  sequencerType  ( 'INPUT:' + s.name() , Members = lst ) 
            self._algos.append  ( _seq )
            
        _seqi = sequencerType( 'MERGEDINPUTS:'+self.name() ,
                               Members      = self._algos  ,
                               ModeOR       = True         , 
                               ShortCircuit = False        )
        
        self._algos2 = [ _seqi , self._sel.algorithm() ]

        _alg = sequencerType('MERGED:' + self.name() , Members = self._algos2 ) 
        
        SelectionBase.__init__( self                        ,
                                algorithm          = _alg   ,
                                outputLocation     = self._sel.outputLocation() ,
                                requiredSelections = []     )


# =============================================================================
## Selection wrapper class for merging output of various Selections.
#  Can be used just like any Selection object.
#  Example:
#  @code     
#  >>> from PhysSelPython.Wrappers import MergedSelection, AutomaticData, SelectionSequence
#  >>> sel00 = AutomaticData(Location = 'Phys/Sel00')
#  >>> sel01 = AutomaticData(Location = 'Phys/Sel01')
#  >>> sel02 = AutomaticData(Location = 'Phys/Sel02')
#  >>> sel03 = AutomaticData(Location = 'Phys/Sel03')
#  >>> merge = MergedSelection('SelMerger', RequiredSelections = [sel00, sel01, sel02, sel03])
#  >>> seqMerge = SelectionSequence('SeqMergeSelections, TopSelection = merge)
#  @endcode 
#  This will OR all the selections and place the output in merge.outputLocation().
class MergedSelectionXXX(NamedObject,
                      ClonableObject,
                      SelectionBase) :
    """Selection wrapper class for merging output of various Selections.
    Can be used just like any Selection object.
    Example:
    
    from PhysSelPython.Wrappers import MergedSelection, AutomaticData, SelectionSequence
    sel00 = AutomaticData(Location = 'Phys/Sel00')
    sel01 = AutomaticData(Location = 'Phys/Sel01')
    sel02 = AutomaticData(Location = 'Phys/Sel02')
    sel03 = AutomaticData(Location = 'Phys/Sel03')
    merge = MergedSelection('SelMerger', RequiredSelections = [sel00, sel01, sel02, sel03])
    seqMerge = SelectionSequence('SeqMergeSelections, TopSelection = merge)

    This will OR all the selections and place the output in merge.outputLocation().
    """
    def __init__(self,
                 name,
                 RequiredSelections = [],
                 OutputBranch = "Phys",
                 sequencerType=Configurables.GaudiSequencer) :

        NamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())

        self._sel = Selection(name,
                              Algorithm = ConfigurableGenerators.FilterDesktop(Code='ALL'),
                              RequiredSelections = RequiredSelections,
                              OutputBranch = OutputBranch)
        
        self._selections = RequiredSelections 
        
        ## it was 
        #self._algos1 = flatAlgorithmList(self._sel)

        ## it now 
        self._algos = [] 
        for s in RequiredSelections :
            lst = flatAlgorithmList ( s )
            if   not lst : continue 
            elif 1 == len ( lst ) :
                self._algos.append( lst[0] ) 
            else :
                _seq  =  sequencerType  ( 'SEQ:' + s.name() , Members = lst ) 
                self._algos.append  ( _seq )
                
        self._algos += [ self._sel.algorithm() ]

        _alg = sequencerType('Seq'+self.name(),
                             Members = self._algos,
                             ModeOR = True,
                             ShortCircuit = False)

        SelectionBase.__init__(self,
                               algorithm = _alg,
                               outputLocation = self._sel.outputLocation(),
                               requiredSelections = [])



# =============================================================================
## Wrapper class for offline selection sequence creation.
#  Takes a Selection object
#  corresponding to the top selection algorithm, and recursively uses
#  Selection.requiredSelections to form a GaudiSequencer with all the required
#  selections needed to run the top selection. Can add list of event selection
#  algorithms to be added at the beginning of the sequence, and a list of
#  algorithms to be run straight after the selection algorithms.
#  Wraps SelSequence, simply adding a method sequence() that creates the
#  GaudiSequencer on-demand.
#  Example: selection sequence for A -> B(bb), C(cc). Add pre-selectors alg0
#  and alg1, and counter counter0.
#  - Assume module A2B2bbC2cc defining a Selection object for the decay
#   A -> B(bb), C(cc)
#  @code
#  >>> from A2B2bbC2cc import SelA2B2bbC2cc
#  >>> from PhysSelPython.Wrappers import SelectionSequence
#  >>> SeqA2B2bbC2cc = SelectionSequence( 'SeqA2B2bbC2cc',
#  ...   TopSelection = SelA2B2bbC2cc,
#  ...   EventPreSelector = [alg0, alg1],
#  ...   PostSelectionAlgs = [counter0]   )
#  # use it
#  >>> mySelSeq = SeqA2B2bbC2cc.sequence()
#  >>> dv = DaVinci()
#  >>> dv.UserAlgorithms = [mySelSeq]
#  # Uses selection.SelSequence and selection.flatAlgorithmList
#  >>> help(SelSequence)
#  >>> help(flatAlgorithmList)
#  @endcode 
class SelectionSequence(SelSequence) :
    """Wrapper class for offline selection sequence creation.
    Takes a Selection object
    corresponding to the top selection algorithm, and recursively uses
    Selection.requiredSelections to form a GaudiSequencer with all the required
    selections needed to run the top selection. Can add list of event selection
    algorithms to be added at the beginning of the sequence, and a list of
    algorithms to be run straight after the selection algorithms.
    Wraps SelSequence, simply adding a method sequence() that creates the
    GaudiSequencer on-demand.
    Example: selection sequence for A -> B(bb), C(cc). Add pre-selectors alg0
             and alg1, and counter counter0.

    # Assume module A2B2bbC2cc defining a Selection object for the decay
    # A -> B(bb), C(cc)
    from A2B2bbC2cc import SelA2B2bbC2cc
    from PhysSelPython.Wrappers import SelectionSequence
    SeqA2B2bbC2cc = SelectionSequence( 'SeqA2B2bbC2cc',
                                       TopSelection = SelA2B2bbC2cc,
                                       EventPreSelector = [alg0, alg1],
                                       PostSelectionAlgs = [counter0]   )
    # use it
    mySelSeq = SeqA2B2bbC2cc.sequence()
    dv = DaVinci()
    dv.UserAlgorithms = [mySelSeq]

    Uses selection.SelSequence and selection.flatAlgorithmList
    help(SelSequence)
    help(flatAlgorithmList)
    """
    __author__ = "Juan Palacios juan.palacios@nikhef.nl"

    def __init__(self,
                 name,
                 TopSelection,
                 EventPreSelector = [],
                 PostSelectionAlgs = [],
                 sequencerType = Configurables.GaudiSequencer) :

        checkName(name)
        SelSequence.__init__(self,
                             name,
                             TopSelection,
                             EventPreSelector,
                             PostSelectionAlgs,
                             sequencerType)

    def outputLocations(self) :
        return [self.outputLocation()]

# =============================================================================
## Wrapper class for offline multiple selection sequence creation.
#  Takes a list of SelectionSequence objects and produces on demand a
#  sequence with an OR of the list of sequences.
class MultiSelectionSequence(UniquelyNamedObject,
                             ClonableObject) :
    """Wrapper class for offline multiple selection sequence creation.
    Takes a list of SelectionSequence objects and produces on demand a
    sequence with an OR of the list of sequences. 
    Uses SelectionSequence:
    help(SelSequence)
    """
    __author__ = "Juan Palacios juan.palacios@nikhef.nl"

    def __init__(self,
                 name,
                 Sequences = [],
                 sequencerType = Configurables.GaudiSequencer) :

        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        self._sequences = list(Sequences)
        self._algos = []
        for seq in self._sequences :
            self._algos += seq.algorithms()

        checkName(self.name())
        self._gaudiseq = sequencerType(self.name(),
                                      ModeOR = True,
                                      ShortCircuit = False,
                                      Members = [seq.sequence() for seq in self._sequences])

    def algorithms(self) :
        return list(self._algos)
        
    def outputLocations(self) :
        return [seq.outputLocation() for seq in self._sequences]

    def sequence(self) :
        return self._gaudiseq

# =============================================================================
## Simple wrapper class to construct charged ProtoParticles. Can add PID
#  information for Rich, Muon, Ecal, Brem, Hcal, Prs, Spd and Velo.
#  Usage:
#  @code
#  >>> tracks = AutomaticData(Location='Some/Location/With/Tracks')
#  >>> ppSel  = ChargedProtoParticleSelection( 'TestPP',
#  ...    RequiredSelections=[tracks],
#  ...    PIDElements=['Rich', 'Muon', 'Hcal'])
#  @endcode 
class ChargedProtoParticleSelection(UniquelyNamedObject,
                                    ClonableObject,
                                    SelectionBase) :

    """Simple wrapper class to construct charged ProtoParticles. Can add PID
    information for Rich, Muon, Ecal, Brem, Hcal, Prs, Spd and Velo.
    Usage:
    tracks = AutomaticData(Location='Some/Location/With/Tracks')
    ppSel = ChargedProtoParticleSelection('TestPP', RequiredSelections=[tracks],  PIDElements=['Rich', 'Muon', 'Hcal'])
    """
    __allowedPIDs = ('Rich',
                     'Muon',
                     'Ecal',
                     'Brem',
                     'Hcal',
                     'Prs',
                     'Spd',
                     'Velo')

    def __init__(self,
                 name,
                 RequiredSelections = [],
                 OutputBranch="Rec",
                 Extension="ProtoParticles",
                 InputDataSetter="Inputs",
                 PIDElements= []) :

        checkName(name)
        checkName(name+'PPMaker')
        self.checkPIDElements(PIDElements)
        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())

        _outputLocation = makeOutputLocation(name = self.name(),
                                             branch    = OutputBranch,
                                             leaf      = Extension)

        _ppMaker = Configurables.ChargedProtoParticleMaker(name+'PPMaker')
        _ppMaker.Output = _outputLocation
        _inputs =  [sel.outputLocation() for sel in RequiredSelections]
        _ppMaker.__setattr__(InputDataSetter, _inputs)

        alg = ConfigurableGenerators.GaudiSequencer(Members=[_ppMaker])
        
        for pid in PIDElements :
            _pidAlgName='ChargedProtoParticleAdd'+pid+'Info'
            _conf = getattr(Configurables, _pidAlgName)(name+pid+'Info')
            alg.Members+= [_conf]
        _dll = Configurables.ChargedProtoCombineDLLsAlg(name+'CombDLLs')
        alg.Members+=[_dll]
        
        SelectionBase.__init__(self,
                               algorithm =alg( self.name() ) ,
                               outputLocation=_outputLocation,
                               requiredSelections = RequiredSelections )

        

    def checkPIDElements(self, pids) :
        for pid in pids :
            if pid not in ProtoParticleSelection.__allowedPIDs :
                raise Exception(pid+" not in allowed set of PID infos")

            
# ========================================================================
## helper shortcut fot creation of 1-step selection
#
#  It is very simple stuff, but in practice it does save a lot of typing!
# 
#  E.g. for 1-step filtering:
#
#  @code
#
#  from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
#  from StandardParticles                     import StdAllLooseMuons as muons 
#  from PhysSelPython.Wrappers                import SimpleSelection
#
#  good_muons = SimpleSelection (
#       'GoodMu'          ,
#       FilterDesktop     ,
#       input = [ muons ] ,
#       Code  = " PT>1 * GeV "  ## will be transferred to the algorithm 
#       )
#
#  @endcode
# 
#  For 1-step decay 
#
#  @code
#
#  from GaudiConfUtils.ConfigurableGenerators import CombineParticles 
#  from StandardParticles                     import StdAllLooseMuons as muons 
#  from PhysSelPython.Wrappers                import SimpleSelection
#
#  dimuons = SimpleSelection (
#       'DiMu'           ,
#       CombineParticles ,
#       input = [ muons ] ,
#       ## the rest of parameters are sent to athe algorithm:
#       DecayDescriptor = " J/psi(1S) -> mu+ mu-" ,
#       CombinationCut  = " in_range( 3 * GeV  , AM , 3.2 * GeV )  " ,
#       MotherCut       = " in_range( 3 * GeV  ,  M , 3.2 * GeV )  " ,
#       )
#
#  @endcode
# 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-02-12
# 
#  @param name          unique selection name 
#  @param algotype      type of algorithm to be used
#  @param inputs        list of input/required selection
#  @param args          additional arguments to be used for algorithm
#  @param kwargs        additional arguments to be used for algorithm
#  @return the selection object 
def SimpleSelection (
    name     ,   ## unique selection name 
    algotype ,   ## type of algorithm to be used
    inputs   ,   ## list of input/required selections
    *args    ,   ## additional arguments to be used for algorithm
    **kwargs ) : ## additional arguments to be used for algorithm
    """Helper shortcut fot creation of 1-step selection

    It is very simple stuff, but in practice it does save a lot of typing!
    
    >>> from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
    >>> from StandardParticles                     import StdAllLooseMuons as muons 
    >>> from PhysSelPython.Wrappers                import SimpleSelection
    
    >>> good_muons = SimpleSelection (
    ...  'GoodMu'          ,
    ...  FilterDesktop     ,
    ... input = [ muons ] ,
    ... Code  = ' PT>1 * GeV '  ## will be transferred to the algorithm 
    ... )

    For 1-step decay 
    
    >>> from GaudiConfUtils.ConfigurableGenerators import CombineParticles 
    >>> from StandardParticles                     import StdAllLooseMuons as muons 
    >>> from PhysSelPython.Wrappers                import SimpleSelection

    >>> dimuons = SimpleSelection (
    ...     'DiMu'           ,
    ...      CombineParticles ,
    ...      input = [ muons ] ,
    ...      ## the rest of parameters are sent to athe algorithm:
    ...      DecayDescriptor = ' J/psi(1S) -> mu+ mu- ' ,
    ...      CombinationCut  = ' in_range( 3 * GeV  , AM , 3.2 * GeV )  ' ,
    ...      MotherCut       = ' in_range( 3 * GeV  ,  M , 3.2 * GeV )  ' ,
    ... )    
    """
    ## get selection's properties: 
    output_branch = kwargs.pop ( 'OutputBranch'     , 'Phys'      )
    input_setter  = kwargs.pop ( 'InputDataSetter'  , 'Inputs'    ) 
    output_setter = kwargs.pop ( 'OutputDataSetter' , 'Output'    ) 
    extension     = kwargs.pop ( 'Extension'        , 'Particles' ) 
    #
    ## create new algorithm or algorithm generator 
    #
    alg = algotype ( *args , **kwargs )
    #
    ## adjust inputs 
    if not isinstance ( inputs , ( list , tuple ) ) : inputs = [ inputs ] 
    #
    ## create selection 
    return Selection (
        name                               , 
        Algorithm          = alg           ,
        RequiredSelections = inputs        ,
        OutputBranch       = output_branch ,
        InputDataSetter    = input_setter  ,
        OutputDataSetter   = output_setter ,
        Extension          = extension             
        )

# =========================================================================
## helper utility to create 'Print'-selection, useful for debugging
#  Such object can be easily inserted into selection flow
#
#  @code 
#
#  ## some input selection
#  selection =
#    
#  ## add ``Printer''
#  selection = PrintSelection ( selection )
#    
#  @endcode 
#
#  @param input           input selection
#  @param printer         printer algorithm type
#  @param InputDataSetter davinci....
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2015-03-18
def PrintSelection (
    input                      ,    ## input selection 
    printer         =  None    ,    ## printer algorthm type
    InputDataSetter = 'Inputs' ,    ## it is just DaVinci. 
    *args                      ,    ## algorithm parameters 
    **kwargs                   ) :  ## algorithm parameters 
    """Helper utility to create 'Print'-selection, useful for debugging
    Such object can be easily inserted into selection flow
    
    >>> ## some input selection
    >>> selection =
    
    >>> add ``Printer''
    >>> selection = PrintSelection ( selection )    
    """
    if printer is None :
        from GaudiConfUtils.ConfigurableGenerators import PrintDecayTree as _Printer_
        printer = _Printer_ 

    #
    ## create new "algorithm"
    #
    algo = printer ( *args , **kwargs )
    
    name = '%s_PRINT' % input.name() 
    #
    ## finally construct valid "Selection"
    #
    return PassThroughSelection (
        name                                ,
        Algorithm         = algo            ,
        RequiredSelection = input           ,
        InputDataSetter   = InputDataSetter )

# =========================================================================
## special type of PassThroughSelection, that
#  "limits" the size of selection
#  @code
#  my_selection = ....
#  my_selection_limited = LimitedSelection ( my_selection , maxsize = 100 )
#  another_selection = XXXSelection ( 'another_selection' ,
#                                       ... ,
#                       RequiredSelections = [ my_selection_limited ] )
#  @endcode
#  @see PassThroughSelection
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-02-10
def LimitSelection ( input               ,                     
                     maxsize             ,
                     minsize      = 1    , 
                     UseRootInTES = True ) :
    """Special type of PassThroughSelection, that
    'limits' the size of selection
    >>> my_selection = ....
    >>> my_selection_limit = LimitSelection ( my_selection , maxsize = 100 )
    >>> another_selection  = XXXSelection ( 'another_selection' ,
    ...                                        ... ,
    ...                 RequiredSelections = [ my_selection_limit ] )
    """
    if 1 == minsize :
        name = '%s_LIMIT_%d'
        code = "CONTAINS('%s',%s)<=%d"
        ##
        name = name  % ( input.name()                          , maxsize )
        code = code  % ( input.outputLocation() , UseRootInTES , maxsize )
    else:
        name = '%s_LIMIT_%d_%d'
        code = "in_range(%d,CONTAINS('%s',%s),%d)"
        ##
        name = name  % ( input.name()           , minsize , maxsize )
        code = code  % ( minsize ,
                         input.outputLocation() , 
                         UseRootInTES           , maxsize )
    ##
    from GaudiConfUtils.ConfigurableGenerators import LoKi__VoidFilter as _VFilter_
    return PassThroughSelection (
        name                                         ,
        Algorithm         = _VFilter_(
        Code = code , Preambulo = ['from LoKiCore.functions import in_range'] ,
        ) ,
        RequiredSelection = input                    )


# ========================================================================#
## special type of PassThroughSelection, that used CheckPV algorithm
#  @code
#  my_selection = ....
#  my_selection_pv   = CheckPVSelection ( my_selection , MinPVs = 1 )
#  another_selection = XXXSelection ( 'another_selection' ,
#                                       ... ,
#                       RequiredSelections = [ my_selection_pv ] )
#  @endcode
#  @see PassThroughSelection
#  @see LimitSelection
#  @see CheckPV 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-03-11
def CheckPVSelection ( input         ,                     
                       MinPVs   =  1 ,
                       MaxPVs   = -1 ,
                       **kwargs      ) :
    """Special type of PassThroughSelection, that used CheckPV algorithm 
    >>> my_selection = ....
    >>> my_selection_pv = LimitSelection ( my_selection , MinPVs = 1  )
    >>> another_selection  = XXXSelection ( 'another_selection' ,
    ...                                        ... ,
    ...                 RequiredSelections = [ my_selection_pv ] )
    """
    if   0 <= MinPVs <= MaxPVs : 
        name = 'CHECKPV_%d_%d' %  ( MinPVs , MaxPVs )
    elif 0 <= MinPVs : 
        name = 'CHECKPV_%d'    %    MinPVs
    else :
        raise TypeError, 'CheckPV: invalid setting: %s/%s' %( MinPVs, MaxPVs ) 
    ##
    from GaudiConfUtils.ConfigurableGenerators import CheckPV as _CHECKPV_
    return PassThroughSelection (
        name                        ,
        Algorithm         = _CHECKPV_ ( MinPVs = MinPVs , MaxPVs = MaxPVs , **kwargs ) ,
        RequiredSelection = input   )

# =============================================================================
## Useful shortcut for selection with FilterDesktop algorithm
#  @code
#  from StandardParticles import StdAllLoosePions   as pions
#  good = FilterSelection (
#      'good'            , ## unique name 
#      [ pions ] , ## required selections
#      Code = '500*MeV<PT'
#      )
#  @endcode 
#  @see SimpleSelection
#  @see Selection
#  @see SelPy.Selection
#  @see FilterDesktop 
#
#  @param name           unique selection name 
#  @param inputs         list of input/required selection
#  @param Code           'Code' property of FilterDesktop
#  @return the selection object
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2016-03-10 
def FilterSelection ( name     ,
                      inputs   ,
                      Code     , 
                      **kwargs ) :
    """ Useful shortcut for selection with FilterDesktop algorithm
    >>> from StandardParticles import StdAllLoosePions   as pions
    >>> good = FilterSelection (
    ... 'good'    , ## unique name 
    ... [ pions ] , ## required selections
    ... Code   =  '500*MeV<PT'
    ... )
    """
    #
    from GaudiConfUtils.ConfigurableGenerators import FilterDesktop as _FILTER_    
    ## create selection    
    return SimpleSelection ( name        ,
                             _FILTER_    ,
                             inputs      ,
                             Code = Code , **kwargs )


# =============================================================================
## Useful shortcut to check validity of the associated best primary vertex 
#  @code
#  my_selection = ...
#  good = ValidBPVSelection (
#      [ my_selection ]  , ## required selections
#      )
#  @endcode 
#  @see FilterSelection
#  @see SimpleSelection
#  @see Selection
#  @see SelPy.Selection
#  @see FilterDesktop 
#
#  @param inputs         list of input/required selection
#  @param name           unique selection name 
#  @return the selection object
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2017-01-17 
def ValidBPVSelection ( inputs     ,
                        name  = '' ,
                        **kwargs   ) :
    """ Useful shortcut to check validity of the associated best primary vertex 
    >>> my_selection = ... 
    >>> good = ValidBPVSelection (
    ... 'good'           , ## unique name 
    ... [ my_selection ] , ## required selections
    ... )
    """
    ## if name is not specified, create it
    if not name :
        if hasattr ( inputs , 'name' ) : 
            name = '%s_VALID_BPV'  % inputs.name() 
        elif 1 == len ( inputs ) :
            name = '%s_VALID_BPV'  % inputs[0].name() 
            
    ## create selection    
    return FilterSelection ( name                ,
                             inputs              ,
                             Code = 'BPVVALID()' , ## NB: the actual code here 
                             **kwargs            )

# =============================================================================
## Useful shortcut for selection CombineParticles  algorithm
#  @code
#  from StandardParticles import StdAllLoosePions   as pions
#  from StandardParticles import StdAllLooseKaons   as kaons
#  charm = CombineSelection (
#     'charm'            , ## unique name 
#      [ pions , kaons ] , ## required selections
#      DecayDescriptor  = '[ D0 -> K- pi+]cc'    , 
#      CombinationCut   = 'in_range  (1.6 * GeV , AM , 2.0 * GeV) ' ,
#      MotherCut        = 'in_range  (1.6 * GeV ,  M , 2.0 * GeV) & ( CHI2VX < 10 ) ' ,
#      )
#  @endcode 
#  @see SimpleSelection
#  @see Selection
#  @see SelPy.Selection
#  @see CombineParticles 
#
#  @param name           unique selection name 
#  @param inputs         list of input/required selection
#  @param CombinationCut 'CombinationCut'-property of CombineParticles 
#  @param MotherCut           'MotherCut'-property of CombineParticles 
#  @param kwargs          additional arguments to be used for algorithm
#  @return the selection object
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2016-03-10 
def CombineSelection ( name  , inputs ,
                       CombinationCut ,
                       MotherCut      , 
                       **kwargs       ) :
    """ Useful shortcut for selection with CombineParticles algorithm
    >>> from StandardParticles import StdAllLoosePions   as pions
    >>> from StandardParticles import StdAllLooseKaons   as kaons
    >>> charm = CombineSelection (
    ... 'charm'            , ## unique name 
    ... [ pions , kaons ] , ## required selections
    ... DecayDescriptor  = '[ D0 -> K- pi+]cc'    , 
    ... CombinationCut   = 'in_range  (1.6 * GeV , AM , 2.0 * GeV) ' ,
    ... MotherCut        = 'in_range  (1.6 * GeV ,  M , 2.0 * GeV) & ( CHI2VX < 10 ) ' ,
    ... )
    """
    #
    from GaudiConfUtils.ConfigurableGenerators import CombineParticles as _COMBINER_
    ## check decay descriptor(s)
    if not kwargs.has_key ('DecayDescriptor' ) : 
        if not kwargs.has_key ('DecayDescriptors' ) :
            raise TypeError, 'CombineSelection: DecayDescriptor(s) must be specified!'
    
    ## create selection    
    return SimpleSelection ( name   , _COMBINER_ ,
                             inputs ,
                             CombinationCut = CombinationCut ,
                             MotherCut      = MotherCut      , **kwargs )

# =============================================================================
## Useful shortcut for selection with creation of 3-body decays
#  @code
#  from StandardParticles import StdAllLoosePions   as pions
#  from StandardParticles import StdAllLooseKaons   as kaons
#  charm = Combine3BodySelection (
#     'charm'            , ## unique name 
#      [ pions , kaons ] , ## required selections
#      DecayDescriptor  = '[ D+ -> K- pi+ pi+]cc'    , 
#      Combination12Cut = ' ( AM < 1.8 * GMeV ) & ( ACHI2DOCA(1,2)<10 ) ' ,
#      CombinationCut   = ''' in_range  (1.6 * GeV , AM , 2.0 * GeV)
#                         & ( ACHI2DOCA(1,3) < 10 )  
#                         & ( ACHI2DOCA(2,3) < 10 )  
#                         ''',
#      MotherCut        = 'in_range  (1.6 * GeV ,  M , 2.0 * GeV) & ( CHI2VX < 10 ) ' ,
#      )
#  @endcode 
#  @see SimpleSelection
#  @see Selection
#  @see SelPy.Selection
#  @see DaVinci::N3BodyDecays
#
#  @param name              unique selection name 
#  @param inputs           list of input/required selection
#  @param Combination12Cut 'Combination12Cut'-property of DaVinci::N3BodyDecays 
#  @param CombinationCut     'CombinationCut'-property of DaVinci::N3BodyDecays
#  @param MotherCut               'MotherCut'-property of DaVinci::N3BodyDecays
#  @param kwargs          additional arguments to be used for algorithm
#  @return the selection object
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2016-03-10 
def Combine3BodySelection ( name  , inputs   ,
                            Combination12Cut ,
                            CombinationCut   ,
                            MotherCut        , 
                            **kwargs         ) :
    """ Useful shortcut for selection with  DaVinci::N3BodyDecays algorithm
    >>> from StandardParticles import StdAllLoosePions   as pions
    >>> from StandardParticles import StdAllLooseKaons   as kaons
    >>> charm = Combine3BodySelection (
    ... 'charm'            , ## unique name 
    ... [ pions , kaons ] , ## required selections
    ... DecayDescriptor  = '[ D+ -> K- pi+ pi+]cc'    , 
    ... Combination12Cut = ' ( AM < 500 * MeV ) & ( ACHI2DOCA(1,2)<10 ) ' ,
    ... CombinationCut   = '''in_range  (1.6 * GeV , AM , 2.0 * GeV)
    ...                       & ( ACHI2DOCA(1,3)<10 ) 
    ...                       & ( ACHI2DOCA(2,3)<10 ) 
    ...                    ''' ,
    ... MotherCut        = 'in_range  (1.6 * GeV ,  M , 2.0 * GeV) & ( CHI2VX < 10 ) ' ,
    ... )
    """
    #
    from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays as _COMBINER_
    ## check decay descriptor(s)
    if not kwargs.has_key ('DecayDescriptor' ) : 
        if not kwargs.has_key ('DecayDescriptors' ) :
            raise TypeError, 'Combine3BodySelection: DecayDescriptor(s) must be specified!'
        
    ## create selection    
    return SimpleSelection ( name   , _COMBINER_ ,
                             inputs ,
                             Combination12Cut = Combination12Cut ,
                             CombinationCut   = CombinationCut   ,
                             MotherCut        = MotherCut        , **kwargs )


# =============================================================================
## Useful shortcut for selection with creation of 4-body decays
#  @code
#  from StandardParticles import StdAllLoosePions   as pions
#  from StandardParticles import StdAllLooseKaons   as kaons
#  charm = Combine4BodySelection (
#     'charm'            , ## unique name 
#      [ pions , kaons ] , ## required selections
#      DecayDescriptor   = '[ D+ -> K- pi+ pi+ pi-]cc'    , 
#      Combination12Cut  = ' ( AM < 1.8 * GeV )  & ( ACHI2DOCA(1,2)<10 ) ' ,
#      Combination123Cut = '''( AM < 1.8 * GeV )
#                              & ( ACHI2DOCA(1,3)<10 )
#                              & ( ACHI2DOCA(2,4)<10 )
#                          ''', 
#      CombinationCut    = ''' in_range  (1.6 * GeV , AM , 2.0 * GeV)
#                         & ( ACHI2DOCA(1,4) < 10 )  
#                         & ( ACHI2DOCA(2,4) < 10 )  
#                         & ( ACHI2DOCA(3,4) < 10 )  
#                         ''',
#      MotherCut        = 'in_range  (1.6 * GeV ,  M , 2.0 * GeV) & ( CHI2VX < 10 ) ' ,
#      )
#  @endcode 
#  @see SimpleSelection
#  @see Selection
#  @see SelPy.Selection
#  @see DaVinci::N4BodyDecays
#
#  @param name              unique selection name 
#  @param inputs            list of input/required selection
#  @param Combination123Cut 'Combination123Cut'-property of DaVinci::N4BodyDecays 
#  @param Combination12Cut   'Combination12Cut'-property of DaVinci::N4BodyDecays 
#  @param CombinationCut       'CombinationCut'-property of DaVinci::N4BodyDecays
#  @param MotherCut                 'MotherCut'-property of DaVinci::N4BodyDecays
#  @param kwargs          additional arguments to be used for algorithm
#  @return the selection object
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2016-03-10 
def Combine4BodySelection ( name  , inputs    ,
                            Combination12Cut  ,
                            Combination123Cut ,
                            CombinationCut    ,
                            MotherCut         , 
                            **kwargs          ) :
    """ Useful shortcut for selection with  DaVinci::N3BodyDecays algorithm
    >>> from StandardParticles import StdAllLoosePions   as pions
    >>> from StandardParticles import StdAllLooseKaons   as kaons
    >>> charm = Combine4BodySelection (
    ... 'charm'            , ## unique name 
    ... [ pions , kaons ] , ## required selections
    ... DecayDescriptor   = '[ D+ -> K- pi+ pi+ pi-]cc'    , 
    ... Combination12Cut  = '  ( AM < 1.8 * GeV ) & ( ACHI2DOCA(1,2)<10 ) ' ,
    ... Combination123Cut = '''( AM < 1.8 * GeV )
    ...                        & ( ACHI2DOCA(1,3)<10 )
    ...                        & ( ACHI2DOCA(2,4)<10 )
    ...                     ''', 
    ... CombinationCut    = ''' in_range  (1.6 * GeV , AM , 2.0 * GeV)
    ...                        & ( ACHI2DOCA(1,4) < 10 )  
    ...                        & ( ACHI2DOCA(2,4) < 10 )
    ...                        & ( ACHI2DOCA(3,4) < 10 )  
    ...                     ''',
    ... MotherCut        = 'in_range  (1.6 * GeV ,  M , 2.0 * GeV) & ( CHI2VX < 10 ) ' ,
    ... )
    """
    #
    from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays as _COMBINER_
    ## check decay descriptor(s)
    if not kwargs.has_key ('DecayDescriptor' ) : 
        if not kwargs.has_key ('DecayDescriptors' ) :
            raise TypeError, 'Combine4BodySelection: DecayDescriptor(s) must be specified!'
    
    ## create selection    
    return SimpleSelection ( name   , _COMBINER_ ,
                             inputs ,
                             Combination12Cut  = Combination12Cut  ,
                             Combination123Cut = Combination123Cut ,
                             CombinationCut    = CombinationCut    ,
                             MotherCut         = MotherCut         , **kwargs )


# =============================================================================
## Useful shortcut for "selection" bases on DecayTreeTuple 
#  @code
#  charm    = .... ## some selection object 
#  my_tuple = TupleSelection (
#     'charm'            , ## unique name 
#      [ charm ]         , ## required selections
#      Decay             = '[ D+ -> ^K- ^pi+ ^pi+]CC' ,
#      Branches          = { ... } , 
#      ToolList          = [ ... ]      
#      )
#  # get the congigurable (for subsequent modification, if needed)
#  algo = mu_tuple.algorithm()
#  # make use of nice decoration yb Rob Lambert 
#  tool1 = algo.addTupleTool ( .....  )
#  ...
#  @endcode
#
#  Special keyword arguments  <code>'MCTools'</code> and/or
#  <code>'MCToolList'</code> are *NOT* delegated to DecayTreeTuple,
#  but (if present) are converted to following equivalent code:
#  @code
#  from Configurables import TupleToolMCTruth 
#  tool = algorithm.addTupleTool( TupleToolMCTruth )
#  tool.ToolList = [ ... here is the list from MCTools/MCToolList ... ] 
#  @endcode
#
#  @see SimpleSelection
#  @see Selection
#  @see SelPy.Selection
#  @see DecayTreeTuple
#  @see TupleToolMCTruth 
#
#  @param name              unique selection name 
#  @param inputs            list of input/required selection
#  @param Decay             'Decay'-property of DecayTreeTuple 
#  @param kwargs          additional arguments to be used for algorithm
#  @return the selection object
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2016-03-10 
def TupleSelectionOLD ( name     ,
                        inputs   ,
                        Decay    , 
                        **kwargs ) :
    """ Useful shortcut for selection with DecayTreeTuple as algorithm 
    >>> charm    = .... ## some selection object 
    >>> my_tuple = TupleSelection (
    ...            'charm'          , ## unique name 
    ...           [ charm ]         , ## required selections
    ...             Decay             = '[ D+ -> ^K- ^pi+ ^pi+]CC' ,
    ...             Branches          = {} , 
    ...             ToolList          = [ ... ]      
    ...             )
    >>> algo = mu_tuple.algorithm()
    ##  make use of nice decoration by Rob Lambert 
    >>> tool1 = algo.addTupleTool ( .....  )
    Special keyword arguments 'MCTools' and/or
    'MCToolList' are *NOT* delegated to DecayTreeTuple,
    but (if present) are converted to following equivalent code:
    >>> from Configurables import TupleToolMCTruth 
    >>> tool = algorithm.addTupleTool( TupleToolMCTruth )
    >>> tool.ToolList = [ ... here is the list from MCTools/MCToolList ... ] 
    """
    #
    from   GaudiConfUtils.ConfigurableGenerators import DecayTreeTuple as _TUPLE_
    ## make use of nice decoration by Rob Lambert:
    import DecayTreeTuple.Configuration
    ##
    mc_tools = kwargs.pop ( 'MCTools'    , []      )
    mc_tools = kwargs.pop ( 'MCToolList' , mc_tools )
    # 
    ## create the selection    
    selection = SimpleSelection ( name          ,
                                  _TUPLE_       ,
                                  inputs        ,
                                  Decay = Decay ,
                                  **kwargs      )

    ## automatically add branches 
    branches = kwargs.get( 'Branches', {} )
    for branch in branches :
        algorithm = selection.algorithm()
        from Configurables import TupleToolDecay
        algorithm.addTool ( TupleToolDecay , name = branch )
        
    if mc_tools :
        algorithm = selection.algorithm()
        from Configurables import TupleToolMCTruth 
        tt_mct          = algorithm.addTupleTool ( TupleToolMCTruth )
        tt_mct.ToolList = mc_tools 

    return selection 

# =============================================================================
## Useful shortcut for "selection" bases on DecayTreeTuple 
#  @code
#  charm    = .... ## some selection object 
#  tup_sel  = TupleSelection (
#     'charm'            , ## unique name 
#      charm             , ## required selection
#      Decay             = '[ D+ -> ^K- ^pi+ ^pi+]CC' ,
#      Branches          = {
#       'D' :  ... ,
#       'K' : .... } , 
#      ToolList          = [ ... ] ,
#      MoreTools         = [ 'TupleToolTISTOS' ] ,
#      ...
#      )
#  # explictely add the touple tool: 
#  tool1 = tup_sel.addTupleTool ( ..... )
#  ...
#  # access to already added tool
#  tistos = tup_sel.TupleToolTISTOS
#  ...
#  # get "branches":
#  D = tup_sel.D
#  K = tup_sel.K
#  @endcode
#
#  Three special keyword arguments
#   - <code>'MCTools'</code> and/or
#   - <code>'MCToolList'</code>
#   - <code>'MoreTools'</code>
#  are *NOT* delegated to DecayTreeTuple,
# 
#  but (if present) are converted to following equivalent code for MCTools/MCToolList
#  @code
#  from Configurables import TupleToolMCTruth 
#  tool = tup_sel.addTupleTool( TupleToolMCTruth )
#  tool.ToolList = [ ... here is the list from MCTools/MCToolList ... ]
#  @endcode
#  And equivalent code for <code>MoreTools</code>
#  @code
#  tup_sel = TupleSelection ( ... )
#  tup_sel.ToolList += MoreTools 
#  @endcode 
#
#  @see SimpleSelection
#  @see Selection
#  @see SelPy.Selection
#  @see DecayTreeTuple
#  @see TupleToolMCTruth 
#
#  @param name               Unique selection name 
#  @param RequiredSelection  Required input selection
#  @param Decay             'Decay'-property of DecayTreeTuple 
#  @param Branches           Branches to be booked 
#  @param kwargs             Additional arguments to be used for algorithm
#  @return the selection object
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2016-03-10 
class TupleSelection (NamedObject    ,
                      ClonableObject ,
                      SelectionBase  ) :
    
    """ Useful shortcut for selection with DecayTreeTuple as algorithm 
    >>> charm    = .... ## some selection object 
    >>> my_tuple = TupleSelection (
    ...            'charm'          , ## unique name 
    ...           [ charm ]         , ## required selections
    ...             Decay             = '[ D+ -> ^K- ^pi+ ^pi+]CC' ,
    ...             Branches          = {
    ...                   'D' :  ... ,
    ...                   'K' : .... } , 
    ...             ToolList          = [ ... ]      
    ...             )
    Explicit add of tools:
    >>> tool1 = my_tuple.addTupleTool ( .....  )
    >>> my_tuple.ToolList += [ 'TupleToolTisTos' ]
    Access to tools
    >>> tistos = my_tuple.TupleToolTisTos
    Accss to branches:
    >>> D = my_tuple.D
    >>> K = my_tuple.K
    Three special keyword arguments are not delegated to DecayTreeTuple:
    - 'MCTools' 
    - 'MCToolList'
    - 'MoreTools' 
    are *NOT* delegated to DecayTreeTuple,
    but (if present) are converted to following equivalent code for(MCTools&MCToolList)
    >>> from Configurables import TupleToolMCTruth 
    >>> tool = tup_sel.addTupleTool( TupleToolMCTruth )
    >>> tool.ToolList = [ ... here is the list from MCTools/MCToolList ... ]
    For `MoreTools', the tools from the list will be just appended to ToolList
    """
    def __init__ ( self                  ,
                   name                  , 
                   RequiredSelection     ,
                   Decay                 ,
                   Branches         = {} ,
                   **kwargs              ) :
        
        ## allow some backward compatibility 
        if isinstance ( RequiredSelection , (list,tuple) ) and 1 == len ( RequiredSelection ) :
            RequiredSelection = RequiredSelection[0]
            
        ## 0) initialize important bases classes 
        NamedObject   .__init__(self, name    )
        ClonableObject.__init__(self, locals())

        ## 1) make use of nice decoration by Rob Lambert:
        import DecayTreeTuple.Configuration
        
        ## 2) get the algorithm 
        from   GaudiConfUtils.ConfigurableGenerators import DecayTreeTuple as _TUPLE_

        ## 3) simplify a little bit the treatment of MC 
        mc_tools   = kwargs.pop ( 'MCTools'    , []       )
        mc_tools   = kwargs.pop ( 'MCToolList' , mc_tools )

        ## 4) more tools ? 
        more_tools = kwargs.pop ( 'MoreTools'  , []       )

        ## 5) create helper selection
        self._sel = Selection ( name                                         ,
                                Algorithm          = _TUPLE_ ( Decay = Decay , **kwargs )  ,
                                RequiredSelections = [ RequiredSelection ] )
        
        ## 6) get the algorithm from it 
        alg = self._sel.algorithm()

        ## 7) automatically add branches:
        alg.addBranches( Branches )

        ## 8) add MC-tools
        if mc_tools :
            from Configurables import TupleToolMCTruth 
            tt_mct          = alg.addTupleTool ( TupleToolMCTruth )
            tt_mct.ToolList = mc_tools

        ## 9) add more tools:
        for t in more_tools :
            if not t in alg.ToolList :
                q = alg.addTupleTool    ( t )

        ## intialize the base 
        SelectionBase.__init__( self                                                      ,
                                algorithm          =   alg                                ,
                                outputLocation     =   RequiredSelection.outputLocation() ,
                                requiredSelections = [ RequiredSelection ]                )

    @property 
    def ToolList ( self ) :
        return self.algorithm().ToolList[:]
    
    @ToolList.setter
    def ToolList ( self , lst ) :
        _alg = self.algorithm() 
        for t in lst :
            if t not in _alg.ToolList :
                _alg.addTupleTool ( t )

    ## delegate adding tools to the algorithm
    def addTupleTool  ( self , *args , **kwargs ) :
        """Delegate adding tools to the algorithm
        """
        return self.algorithm().addTupleTool ( *args , **kwargs )
    
    ## delegate adding tools to the algorithm
    def addTool       ( self , *args , **kwargs ) :
        """Delegate adding tools to the algorithm
        """
        return self.addTupleTool ( *args , **kwargs ) 

    ## Use the properties of underlying algorithm as attributes for selection
    def __getattr__ ( self , attr ) :
        """Use the properties of underlying algorithm as attributes for selection
        """
        _alg   = self.algorithm()
        return _alg.getProp( attr )  

# =============================================================================
## @class PayloadSelection
#  Pseudo-selection that allows to "attach" some useful payload
#  (e.g. momentum scaling, mc-link resurrection, or just simple ``Hello,world!''
#  into selection sequence
#  @code
#  selection = ....  ## some selection
#  algorthm  = ...   ## some algorithm ("ready-to-use" Configurable) 
#  selection = PayloadSelection ( selection , algorithm )
#  @endcode
#  Few examples: 
#  @code 
#  from Configurable import LoKi__HelloWorld as HELLO 
#  selection = PayloadSelection ( selection , HELLO('Hello') )  
#  @endcode
#  If decision from Payload algorithm should not be ignored, use switch 'IgnoreDecision = False'
#  
#  @attention "algorithm" is "ready-to-use" configurable here, not a generator
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-02-25
class PayloadSelection(UniquelyNamedObject ,
                       ClonableObject      ,
                       SelectionBase       ) :
    """  Pseudo-selection that allows to embedd some useful payload
    (e.g. momentum scaling, momentum smear, resurrecion of MC-links, or just simple
    ``Hello,world!'' algorithm) into the overall algorithm flow
    
    >>> selection = ...  ## some selection
    >>> algorithm = ...  ## some algorithm (ready-to-use Configurable) 
    >>> selection = PayloadSelection ( selection , algorithm )
    ## one more payload: 
    >>> from Configurable import LoKi__HelloWorld as HELLO
    >>> selection = PayloadSelection ( selection , HELLO('Hello') )  
    Attention: ``algorithm'' is ``ready-to-use'' Configurable here, not a generator
    If decision from Payload algorithm should not be ignored, use switch 'IgnoreDecision = False'
    """
    def __init__( self                           ,
                  RequiredSelection              , 
                  What                           ,
                  IgnoreDecision = True         ,
                  NameFormat     = '%s_PAYLOAD' ) :  

        ## construct more or less unique name 
        try : 
            name = NameFormat 
            name = name % RequiredSelection.name()
        except:
            name = '%s_PAYLOAD'
            name = name % RequiredSelection.name()
            
        UniquelyNamedObject . __init__( self , name     )
        ClonableObject      . __init__( self , locals() )
        
        checkName(self.name())
        
        if IgnoreDecision : 
            from Configurables import GaudiSequencer as SEQUENCER 
            alg = SEQUENCER ( "PAYLOAD:%s_with_%s" % (  RequiredSelection.name() , What.name() ) , 
                              Members            = [ What ]        ,
                              ShortCircuit       = False           ,
                              IgnoreFilterPassed = True            )
        else :
            alg = What 
        
        SelectionBase.__init__( self                     ,
                                algorithm          = alg ,
                                outputLocation     = RequiredSelection.outputLocation(),
                                requiredSelections = [RequiredSelection] )



# =============================================================================
## useful shortcut to create 'TisTos'-selection
#  @code
#  my_selection = ....
#  l0tos        = TisTosSelection ( my_selection           ,
#                                   trigger = 'L0'         ,
#                                   tistos  = 'TOS'        ,
#                                   lines   = 'L0Hadron.*' , ## trigger lines 
#                                   name    = 'Triggered'  )  
#  @endcode
#  If <code>name</code> is not specified, it will be automatically constructed
#  @param trigger trigger level: L0, Hlt1 or Hlt2
#  @param tistos               : 'TIS' , 'TOS' , 'TPS' or 'TUS'
#  @see FilterSelection
#  @see SimpleSelection
def TisTosSelection ( selection      ,
                      trigger        , ## L0, Hlt1 or Hlt2 
                      tistos         , ## TIS, TOS, TPS or TUS 
                      lines          , 
                      name     = ''  , 
                      **kwargs       ) :
    """Useful shortcut to create 'TisTos'-selection
    trigger :  'L0' , 'Hlt1' or 'Hlt1'
    tistos  :  'TIS' , 'TOS' , 'TUS' or 'TPS'
    If ``name'' is not specified, it will be automatically constructed from ``trigger'' and ``tistos''
    >>> my_selection = ....
    >>> l0tos        = TisTosSelection ( my_selection           ,
    ...                                  trigger = 'L0'         ,
    ...                                  tistos  = 'TOS'        ,
    ...                                  lines   = 'L0Hadron.*' , ## trigger lines 
    ... name    = 'Triggered'  )
    """
    #
    utrigger = trigger.upper() 
    if not  utrigger in ( 'L0' , 'HLT1' , 'HLT2' ) :
        raise AttributeError('Invalid Trigger type "%s" for TisTos selection!' % trigger )

    #
    utistos  = tistos.upper() 
    if not utistos in ( 'TIS' , 'TOS' , 'TPS' , 'TUS' ) :
        raise AttributeError('Invalid TisTos  type "%s" for TisTos selection!' % tistos  )

    #
    preambulo =  kwargs.get( 'Preambulo' , [] )
    preambulo . append ( "from LoKiPhys.decorators import *"                                ) 
    preambulo . append ( "from LoKiPhys.functions  import   L0TIS,  L0TOS,  L0TPS,  L0TUS" ) 
    preambulo . append ( "from LoKiPhys.functions  import Hlt1TIS,Hlt1TOS,Hlt1TPS,Hlt1TUS" ) 
    preambulo . append ( "from LoKiPhys.functions  import Hlt2TIS,Hlt2TOS,Hlt2TPS,Hlt2TUS" ) 
    #
    if not name :
        name = "%s_%s%s" % ( selection.name() , utrigger , utistos )
    ##
    if   'L0'   == utrigger : code  = 'L0'
    elif 'HLT1' == utrigger : code  = 'Hlt1'
    elif 'HLT2' == utrigger : code  = 'Hlt2'
    ##
    if   'TIS'  == utistos  : code += 'TIS'
    elif 'TOS'  == utistos  : code += 'TOS'
    elif 'TUS'  == utistos  : code += 'TUS'
    elif 'TPS'  == utistos  : code += 'TPS'
    ##
    code += "('%s')" % lines 
    ##
    return FilterSelection (
        name                  ,
        [ selection ]         ,
        Code      = code      ,
        Preambulo = preambulo ,
        **kwargs             
        )

# =============================================================================
## specialization of TisTosSelection for L0-TOS
#  @code
#  my_selection = ...
#  l0tos = L0TOSSelection ( my_selection , 'L0Hadron.*' ) 
#  @endcode
#  @see TisTosSelection
#  @see FilterSelection
def L0TOSSelection  ( selection  ,
                      lines      ,
                      name  = '' ,
                      **kwargs   ) :
    """Specialization of TisTosSelection for L0-TOS
    >>> my_selection = ...
    >>> l0tos = L0TOSSelection ( my_selection , 'L0Hadron.*' ) 
    see TisTosSelection
    """
    return TisTosSelection ( selection       ,
                             trigger = 'L0'  ,
                             tistos  = 'TOS' ,
                             lines   = lines ,
                             name    = name  , **kwargs )


# =============================================================================
## specialization of TisTosSelection for L0-TIS
#  @code
#  my_selection = ...
#  l0tis = L0TISSelection ( my_selection , 'L0Hadron.*' ) 
#  @endcode
#  @see TisTosSelection
#  @see FilterSelection
def L0TISSelection  ( selection  ,
                      lines      ,
                      name  = '' ,
                      **kwargs   ) :
    """Specialization of TisTosSelection for L0-TIS
    >>> my_selection = ...
    >>> l0tis = L0TISSelection ( my_selection , 'L0Hadron.*' ) 
    see TisTosSelection
    """
    return TisTosSelection ( selection       ,
                             trigger = 'L0'  ,
                             tistos  = 'TIS' ,
                             lines   = lines ,
                             name    = name  , **kwargs )


# =============================================================================
## specialization of TisTosSelection for Hlt1-TOS
#  @code
#  my_selection = ...
#  l1tos = Hlt1TOSSelection ( my_selection , 'Hlt1.*JPsi.*' ) 
#  @endcode
#  @see TisTosSelection
#  @see FilterSelection
def Hlt1TOSSelection  ( selection  ,
                        lines      ,
                        name  = '' ,
                        **kwargs   ) :
    """Specialization of TisTosSelection for Hlt1-TOS
    >>> my_selection = ...
    >>> l1tos = Hlt1TOSSelection ( my_selection , 'Hlt1.*JPsi.*' ) 
    see TisTosSelection
    """
    return TisTosSelection ( selection        ,
                             trigger = 'Hlt1' ,
                             tistos  = 'TOS'  ,
                             lines   = lines  ,
                             name    = name   , **kwargs )

# =============================================================================
## specialization of TisTosSelection for Hlt1-TIS
#  @code
#  my_selection = ...
#  l1tis = Hlt1TISSelection ( my_selection , 'Hlt1.*Muon.*' ) 
#  @endcode
#  @see TisTosSelection
#  @see FilterSelection
def Hlt1TISSelection  ( selection  ,
                        lines      ,
                        name  = '' ,
                        **kwargs   ) :
    """Specialization of TisTosSelection for Hlt1-TOS
    >>> my_selection = ...
    >>> l1tis = Hlt1TISSelection ( my_selection , 'Hlt1.*Muon.*' ) 
    see TisTosSelection
    """
    return TisTosSelection ( selection        ,
                             trigger = 'Hlt1' ,
                             tistos  = 'TIS'  ,
                             lines   = lines  ,
                             name    = name   , **kwargs )

# =============================================================================
## specialization of TisTosSelection for Hlt2-TOS
#  @code
#  my_selection = ...
#  l2tos = Hlt2TOSSelection ( my_selection , 'Hlt2.*JPsi.*' ) 
#  @endcode
#  @see TisTosSelection
#  @see FilterSelection
def Hlt2TOSSelection  ( selection  ,
                        lines      ,
                        name  = '' ,
                        **kwargs   ) :
    """Specialization of TisTosSelection for Hlt2-TOS
    >>> my_selection = ...
    >>> l2tos = Hlt2TOSSelection ( my_selection , 'Hlt2.*JPsi.*' ) 
    see TisTosSelection
    """
    return TisTosSelection ( selection        ,
                             trigger = 'Hlt2' ,
                             tistos  = 'TOS'  ,
                             lines   = lines  ,
                             name    = name   , **kwargs )


# =============================================================================
## specialization of TisTosSelection for Hlt2-TIS
#  @code
#  my_selection = ...
#  l2tis = Hlt2TISSelection ( my_selection , 'Hlt2.*Muon.*' ) 
#  @endcode
#  @see TisTosSelection
#  @see FilterSelection
def Hlt2TISSelection  ( selection  ,
                        lines      ,
                        name  = '' ,
                        *kwargs    ) :
    """Specialization of TisTosSelection for Hlt2-TOS
    >>> my_selection = ...
    >>> l2tis = Hlt2TISSelection ( my_selection , 'Hlt2.*Muon.*' ) 
    see TisTosSelection
    """
    return TisTosSelection ( selection        ,
                             trigger = 'Hlt2' ,
                             tistos  = 'TIS'  ,
                             lines   = lines  ,
                             name    = name   , **kwargs )

# =============================================================================
## @class MomentumSmearing
#  Pseudo-selection that allows to embedd the momentum
#  scaling algorithm into the overall flow
#  @code
#  selection = ....  ## some selection
#  selection = MomentumSmear ( selection ) 
#  @endcode
#  @see TrackSmearState
#  @attention it applies momentum smearing for *all* tracks!
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-02-25
class MomentumSmear(PayloadSelection) :
    """Pseudo-selection that allows to embedd the momentum
    scaling algorithm into the overall flow
    
    >>> selection = ....  ## some selection
    >>> selection = MomentumSmear ( selection )
    Attention: it applies momentum smearing for *all* tracks!
    """
    def __init__(self              ,
                 RequiredSelection ) :
        
        from Configurables import TrackSmearState as _SMEAR_         
        smear = _SMEAR_ ('SMEAR')   ## NB: THE NAME is fixed, is COMMON INSTANCE
        
        PayloadSelection.__init__ ( self                        ,
                                    RequiredSelection           ,
                                    smear                       ,
                                    IgnoreDecision = False      , 
                                    NameFormat     = '%s_SMEAR' ) 

# =============================================================================
## Simple wrapper for EventSelection, that allows to select/filter events according
#  to (global) trigger decision
#  @code
#  hlt1  = TriggerSelection( 'MyTrigger', 'Hlt1' , "HLT_PASS_RE('Hlt1.*DiMuon.*')" )
#  pions = ...
#  kaons = ...
#  mysel = SimpleSelection ( 'D0' , [ hlt1 , pions , kaons ] , .... ) 
#  @endcode
#  @see EventSelection 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-11-09
def TriggerSelection ( name          ,
                       trigger       ,   ## MUST be L0, L0DU, Hlt1, Hlt2 or Strip, Stripping  
                       Code          ,   ## the code.... 
                       Location = '' ,   ## location of DecReports
                       *args         ,   ## other properties 
                       **kwargs      ) : ## other properties
    """Simple wrapper for EventSelection, that allows to select/filter events according
    to (global) trigger decision:
    >>> hlt1  = TriggerSelection( 'MyTrigger', 'Hlt1' , \"HLT_PASS_RE('Hlt1.*DiMuon.*')\" )
    >>> pions = ...
    >>> kaons = ...
    >>> mysel = SimpleSelection ( 'D0' , [ hlt1 , pions , kaons ] , .... )    
    """
    
    if kwargs.has_key('Algorithm') :
        raise KeyError ( 'Algorithm is not allowed keyword for TriggerSelection!')

    if   trigger.upper() in ( 'L0' , 'L0DU' ) :
        from GaudiConfUtils.ConfigurableGenerators import LoKi__L0Filter  as _ALGORITHM_
    elif trigger.upper() in ( 'HLT1' , 'HLT2' , 'STRIP' , 'STRIPPING' ) :
        from GaudiConfUtils.ConfigurableGenerators import LoKi__HDRFilter as _ALGORITHM_
    else :
        raise KeyError ( 'Trigger type "%s" is illegal for TriggerSelection' % trigger ) 
 
    if   Location : kwargs[ 'Location' ] = Location
    elif trigger.upper() in ( 'HLT1'  ,             ) : kwargs[ 'Location' ] = 'Hlt1/DecReports'
    elif trigger.upper() in ( 'HLT2'  ,             ) : kwargs[ 'Location' ] = 'Hlt2/DecReports'
    elif trigger.upper() in ( 'STRIP' , 'STRIPPING' ) : kwargs[ 'Location' ] = 'Strip/DecReports'
    
    kwargs[ 'Code'      ] =  Code

    return EventSelection ( name , Algorithm = _ALGORITHM_ ( *args , **kwargs ) )
    
# =============================================================================
## Simple wrapper for TriggerSelection, that allows to select/filter events according
#  to (global) L0 trigger decision
#  @code
#  l0    = L0Selection( 'MyTrigger', "L0_CHANNEL_SUB('Muon')" )
#  pions = ...
#  kaons = ...
#  mysel = SimpleSelection ( 'D0' , [ l0 , pions , kaons ] , .... ) 
#  @endcode 
#  @see Hlt1Selection 
#  @see Hlt2Selection 
#  @see StrippingSelection 
#  @see TriggerSelection 
#  @see EventSelection 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-11-09
def L0Selection ( name , Code , *args , **kwargs ) :
    """Simple wrapper for TriggerSelection, that allows to select/filter events according
    to (global) trigger decision:
    >>> l0    = L0Selection( 'MyTrigger' , \"L0_CHANNEL_SUB('Muon')\" )
    >>> pions = ...
    >>> kaons = ...
    >>> mysel = SimpleSelection ( 'D0' , [ l0 , pions , kaons ] , .... )    
    """
    return TriggerSelection ( name , 'L0' , Code , '' , *args , **kwargs )  
    
# =============================================================================
## Simple wrapper for TriggerSelection, that allows to select/filter events according
#  to (global) trigger decision
#  @code
#  hlt1  = Hlt1Selection( 'MyTrigger', "HLT_PASS_RE('Hlt1.*DiMuon.*')" )
#  pions = ...
#  kaons = ...
#  mysel = SimpleSelection ( 'D0' , [ hlt1 , pions , kaons ] , .... ) 
#  @endcode 
#  @see Hlt2Selection 
#  @see StrippingSelection 
#  @see TriggerSelection 
#  @see EventSelection 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-11-09
def Hlt1Selection ( name , Code , *args , **kwargs ) :
    """Simple wrapper for TriggerSelection, that allows to select/filter events according
    to (global) trigger decision:
    >>> hlt1  = Hlt1Selection( 'MyTrigger' , \"HLT_PASS_RE('Hlt1.*DiMuon.*')\" )
    >>> pions = ...
    >>> kaons = ...
    >>> mysel = SimpleSelection ( 'D0' , [ hlt1 , pions , kaons ] , .... )    
    """
    return TriggerSelection ( name , 'Hlt1' , Code , '' , *args , **kwargs )  
    

# =============================================================================
## Simple wrapper for TriggerSelection, that allows to select/filter events according
#  to (global) trigger decision
#  @code
#  hlt2  = Hlt2Selection( 'MyTrigger', "HLT_PASS_RE('Hlt2.*DiMuon.*')" )
#  pions = ...
#  kaons = ...
#  mysel = SimpleSelection ( 'D0' , [ hlt2 , pions , kaons ] , .... ) 
#  @endcode 
#  @see Hlt2Selection 
#  @see StrippingSelection 
#  @see TriggerSelection 
#  @see EventSelection 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-11-09
def Hlt2Selection ( name , Code , *args , **kwargs ) :
    """Simple wrapper for TriggerSelection, that allows to select/filter events according
    to (global) trigger decision:
    >>> hlt2  = Hlt2Selection( 'MyTrigger' , \"HLT_PASS_RE('Hlt2.*DiMuon.*')\" )
    >>> pions = ...
    >>> kaons = ...
    >>> mysel = SimpleSelection ( 'D0' , [ hlt2 , pions , kaons ] , .... )    
    """
    return TriggerSelection ( name , 'Hlt2' , Code , '' , *args , **kwargs )  


# =============================================================================
## Simple wrapper for TriggerSelection, that allows to select/filter events according
#  to (global) trigger decision
#  @code
#  strip = StrippingSelection( 'MyStripping', "HLT_PASS_RE('.*DiMuon.*')" )
#  pions = ...
#  kaons = ...
#  mysel = SimpleSelection ( 'D0' , [ strip , pions , kaons ] , .... ) 
#  @endcode 
#  @see Hlt1Selection 
#  @see Hlt2Selection 
#  @see TriggerSelection 
#  @see EventSelection 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-11-09
def StrippingSelection ( name , Code , *args , **kwargs ) :
    """Simple wrapper for TriggerSelection, that allows to select/filter events according
    to (global) trigger decision:
    >>> strip = StrippingSelection( 'MyTrigger' , \"HLT_PASS_RE('Hlt2.*DiMuon.*')\" )
    >>> pions = ...
    >>> kaons = ...
    >>> mysel = SimpleSelection ( 'D0' , [ strip , pions , kaons ] , .... )    
    """
    return TriggerSelection ( name , 'Strip' , Code , '' , *args , **kwargs )  



# =============================================================================
## "Rebuild"-selection:
# A way to rerun/rebuild the existing "standard/common" selection, ignoring
# the objects on tape. It is *very* useful for e.g. MC-uDST.
# Essentially it is *vital* for "inclusive" lines in MC-uDST
# @code
# muons = RebuildSelection ( 'StdAllNoPIDsMuons' ) ## by name 
# 
# from CommonParticles.StdLooseProtons import StdLooseProtons
# protons = RebuildSelection ( StdLooseProtons )  ## by algorithm
#
# from StrandardParticles StdLoosePions
# pions = RebuildSelection ( StdLoosePions )  ## by selection
# @endcode
# @see Selection
# @see AutomaticData
# @see CommonParticles
# @see StandardParticles
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date 2016-11-09
def RebuildSelection ( selection , replace = True , prefix = 'REBUILD:%s' ) :
    """ ``Rebuild''-selection:
    A way to rerun/rebuild the existing ``standard/common'' selection, ignoring
    the objects on tape. It is *very* useful for e.g. MC-uDST.
    Essentially it is *vital* for ``inclusive'' lines in MC-uDST
    
    >>> muons = RebuildSelection ( 'StdAllNoPIDsMuons' ) ## by name
    
    >>> from CommonParticles.StdLooseProtons import StdLooseProtons
    >>> protons = RebuildSelection ( StdLooseProtons )  ## by algorithm
    
    >>> from StandardParticles StdLoosePions
    >>> pions = RebuildSelection ( StdLoosePions )  ## by selection
    """
    if isinstance ( selection , AutomaticData ) :
        n = selection .name()
        n = n[:n.find('_Particles')]
        d,r,n = n.rpartition('_')
        if r and n : selection = n
        
    if isinstance ( selection ,  str ) :        
        import importlib
        COMMON    = importlib.import_module ( 'CommonParticles.%s' % selection )
        algorithm = COMMON.algorithm 
        if not algorithm :
            raise AttributeError('No proper algorithm is found!')
    elif isinstance ( selection , Configurable  ) : algorithm = selection 
    else :
        raise AttributeError("Unknown selection type")

    inputs = []
    if hasattr ( algorithm , 'Inputs' ) : 
        for i in algorithm.Inputs :
            ii    = i[:i.rfind('/Particles')]
            d,s,n = ii.rpartition('/')
            if s and n :
                ## recursion starts here
                ss = RebuildSelection ( n , replace , prefix )
                inputs.append ( ss )

    aname = algorithm.name() 
    try : 
        NAME = prefix % aname
    except:
        NAME = prefix + aname


    import StandardParticles as SP
    if not hasattr ( SP , 'rebuild' ) : SP.rebuild = {}
    
    sel = SP.rebuild.get( NAME , None )
    if not sel :
        
        algorithm.Inputs = []
        sel = Selection ( NAME , Algorithm = algorithm , RequiredSelections = inputs )
        
        SP.rebuild [ NAME ] = sel
        
        if replace : setattr ( SP , aname , sel )
            
    return sel

# =============================================================================
if '__main__' == __name__ :

    print   80*'*'  
    print  __doc__ 
    print  ' Author  : %s ' %  __author__  
    print  ' Symbols : %s ' %  list ( __all__ )
    print   80*'*'
    
    from sys import modules
    _this = modules[ __name__ ]
    for o in __all__ :
        obj = getattr ( _this , o , None )
        if obj and obj.__doc__ :
            doc = obj.__doc__.replace('\n', '\n#') 
            print "# %s\n# %s" % ( o , doc ) 
    print   80*'*'  
 
# =============================================================================
# The END 
# =============================================================================
