#ifndef MICRODST_COPYLINEPERSISTENCELOCATIONS_H
#define MICRODST_COPYLINEPERSISTENCELOCATIONS_H 1

#include <functional>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <GaudiAlg/GaudiAlgorithm.h>
#include <GaudiKernel/DataObject.h>

#include <Event/HltDecReports.h>
#include <Kernel/ILinePersistenceSvc.h>
#include <MicroDST/MicroDSTCommon.h>
#include <MicroDST/ICloner.h>

/** @class CopyLinePersistenceLocations CopyLinePersistenceLocations.h
 *
 * @brief Clone TES locations, of KeyedContainer objects, defined by the
 * ILinePersistenceSvc.
 *
 * For each location returned by the ILinePersistenceSvc, if that location
 * contains a KeyedContainer and if the object stored in that KeyedContainer is
 * supported by this algorithm, the container is cloned from its original
 * location to that with `/Event` replaced by `/Event/<prefix>`, where
 * `<prefix>` is the value of the `OutputPrefix` property (inherited from
 * MicroDSTCommon).
 *
 * The objects currently supported for cloning are:
 *
 * * LHCb::CaloHypo, with the cloner defined by the `ICloneCaloHypo` property
 * * LHCb::CaloCluster, with the cloner defined by the `ICloneCaloCluster` property
 * * LHCb::Particle, with the cloner defined by the `ICloneParticle` property
 * * LHCb::ProtoParticle, with the cloner defined by the `ICloneProtoParticle` property
 * * LHCb::VertexBase, with the cloner defined by the `ICloneVertexBase` property
 *
 * To accomodate different cloner types for PersistReco (a.k.a.
 * Turbo++/TurboPP) lines, the `TurboPPICloneVertexBase` property allows the
 * LHCb::VertexBase cloner to be different to that used for 'regular' Turbo
 * lines. The primary use-case is allowing the cloning of PV tracks for Turbo++
 * lines, in addition to the usual cloning of the PVs themselves. It is an
 * assumption of this algorithm that the Turbo++ cloner copies a superset of
 * the information copied by the Turbo cloner.
 *
 * If a TES location defined by the ILinePersistenceSvc contains a
 * KeyedContainer of an object not in the above list, or does not contain a
 * KeyedContainer, the location is skipped (no warning or error is printed).
 *
 * *Note*: This algorithm will use the cloner tools as defined by its various
 * `IClone*` properties, but the cloner tools may themselves use other cloner
 * tools, which need to be overridden explicitly.
 * (For example, ParticleCloner uses an ICloneCaloHypo tool to clone associated
 * LHCb::CaloHypo objects, and setting the cloning tool on _this_ algorithm but
 * not on ParticleCloner may result in different cloning techniques for the
 * same object type.)
 *
 * Example usage:
 *
 * @code
 * from Configurables import CopyLinePersistenceLocations
 * container_cloner = CopyLinePersistenceLocations(
 *     OutputPrefix='/Event/SomeStreamName',
 *     # Clone the locations requested by one Turbo line
 *     LinesToCopy=['Hlt2CharmHadD02KmPipTurbo'],
 *     # Get the list of locations to copy from the TCK
 *     ILinePersistenceSvc='TCKLinePersistenceSvc',
 *     # Use a specific cloner for RecVertex containers. Here we choose
 *     # a cloner that clears the container of tracks on each cloned vertex
 *     ICloneVertexBase='VertexBaseFromRecVertexClonerNoTracks',
 *     # Use a specific cloner for RecVertex containers requested by Turbo++ #
 *     lines. Here we choose a cloner that also copies the container of tracks
 *     # associated to each cloned vertex
 *     TurboPPICloneVertexBase='VertexBaseFromRecVertexClonerNoTracks'
 * )
 * @endcode
 *
 * @see CopyParticle2PVRelationsFromLinePersistenceLocations for cloning
 * relations tables
 */
class CopyLinePersistenceLocations : public MicroDSTCommon<GaudiAlgorithm> {
  public:
    CopyLinePersistenceLocations(const std::string& name, ISvcLocator* pSvcLocator);

    StatusCode initialize() override;

    StatusCode execute() override;

    using CLIDToCloner = std::map<CLID, std::function<void(const DataObject&)>>;

  private:
    /// If true, always create output containers, even if the input locations are empty.
    /// (Either way, an output container is never made if the input location does not exist.)
    Gaudi::Property<bool> m_alwaysCreateOutput{this, "AlwaysCreateOutput", true};

    /// Implementation of ILinePersistenceSvc used to get the list of locations to clone.
    Gaudi::Property<std::string> m_linePersistenceSvcName{this, "ILinePersistenceSvc", "TCKLinePersistenceSvc"};

    /// Implementation of ICloneCaloCluster used to clone LHCb::CaloCluster objects.
    Gaudi::Property<std::string> m_caloClusterClonerName{this, "ICloneCaloCluster", "CaloClusterCloner"};

    /// Implementation of ICloneCaloHypo used to clone LHCb::CaloHypo objects.
    Gaudi::Property<std::string> m_caloHypoClonerName{this, "ICloneCaloHypo", "CaloHypoCloner"};

    /// Implementation of ICloneParticle used to clone LHCb::Particle objects.
    Gaudi::Property<std::string> m_particleClonerName{this, "ICloneParticle", "ParticleCloner"};

    /// Implementation of ICloneProtoParticle used to clone LHCb::ProtoParticle objects.
    Gaudi::Property<std::string> m_protoParticleClonerName{this, "ICloneProtoParticle", "ProtoParticleCloner"};

    /// Implementation of ICloneVertexBase used to clone LHCb::VertexBase objects
    Gaudi::Property<std::string> m_vertexClonerName{this, "ICloneVertexBase", "VertexBaseFromRecVertexClonerNoTracks"};

    /// Implementation of ICloneVertexBase used to clone LHCb::VertexBase objects for Turbo++ lines
    Gaudi::Property<std::string> m_turboPPVertexClonerName{this, "TurboPPICloneVertexBase", "VertexBaseFromRecVertexClonerWithTracks"};

    /// List of HLT2 lines whose outputs are to be copied.
    Gaudi::Property<std::vector<std::string>> m_linesToCopy{this, "LinesToCopy", {}};

    std::set<std::string> m_linesToCopySet;

    /// TES location of the HltDecReports object to give to ILinePersistenceSvc.
    Gaudi::Property<std::string> m_hltDecReportsLocation{this, "Hlt2DecReportsLocation", LHCb::HltDecReportsLocation::Hlt2Default};

    SmartIF<ILinePersistenceSvc> m_linePersistenceSvc;

    /// Map class IDs to cloner tools.
    CLIDToCloner m_cloners;

    /// Map class IDs to cloner tools to be used for locations belonging to Turbo++ lines.
    CLIDToCloner m_turboPPCloners;

    /// Bind the MicroDST::ICloner tool, named cloner_name and that can clone
    /// ClonerSourceObject types, to the KeyedObjectToClone template parameter
    template<typename KeyedObjectToClone, typename ClonerSourceObject = KeyedObjectToClone>
    void registerCloner(const std::string& cloner_name, CLIDToCloner& classMap);

    /// Loop over the list of locations and invoke the cloner lambda on each, dispatching on the
    /// class ID of the KeyedContainer at each location
    void cloneLocations(const std::set<std::string>& locations, const CLIDToCloner& cloners);

    /// Loop over the container contents and call cloner::operator() on each element.
    template<typename KeyedObjectToClone, typename ClonerSourceObject = KeyedObjectToClone>
    void cloneKeyedContainer(const KeyedContainer<KeyedObjectToClone>& container,
                             MicroDST::ICloner<ClonerSourceObject>* cloner) const;
};
#endif // MICRODST_COPYLINEPERSISTENCELOCATIONS_H
