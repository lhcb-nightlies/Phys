#include <sstream>

#include <GaudiKernel/KeyedContainer.h>

#include <Event/CaloCluster.h>
#include <Event/CaloHypo.h>
#include <Event/Particle.h>
#include <Event/ProtoParticle.h>
#include <Event/RecVertex.h>
#include <Event/VertexBase.h>

#include "CopyLinePersistenceLocations.h"

DECLARE_ALGORITHM_FACTORY(CopyLinePersistenceLocations)

CopyLinePersistenceLocations::CopyLinePersistenceLocations(const std::string& name, ISvcLocator* svcLocator)
  : MicroDSTCommon<GaudiAlgorithm>(name, svcLocator) {

  m_linesToCopy.declareUpdateHandler(
    [this](Property&) {
      this->m_linesToCopySet = std::set<std::string>(m_linesToCopy.begin(), m_linesToCopy.end());
    }
  );
  m_linesToCopy.useUpdateHandler();
}

template<typename KeyedObjectToClone, typename ClonerSourceObject/* = KeyedObjectToClone*/>
void CopyLinePersistenceLocations::registerCloner(const std::string& clonerName,
                                                  CopyLinePersistenceLocations::CLIDToCloner& classMap) {
  using KeyedContainerToClone = KeyedContainer<KeyedObjectToClone>;
  using Cloner = MicroDST::ICloner<ClonerSourceObject>;

  // Instantiate the cloner tool for the template type
  auto algTool = tool<IAlgTool>(clonerName, this);
  if (!algTool) {
    const auto errmsg = "Could not retrieve tool " + clonerName;
    throw GaudiException(errmsg, this->name(), StatusCode::FAILURE);
  }
  auto cloner = dynamic_cast<Cloner*>(algTool);
  if (!cloner) {
    const std::string clonerTypeName{typeid(Cloner).name()};
    const auto errmsg = "Failed to cast IAlgTool to " + clonerTypeName;
    throw GaudiException(errmsg, this->name(), StatusCode::FAILURE);
  }

  classMap[KeyedContainerToClone::classID()] =
    [this, cloner](const DataObject& container) {
      const auto& castedContainer = dynamic_cast<const KeyedContainerToClone&>(container);
      this->cloneKeyedContainer<KeyedObjectToClone, ClonerSourceObject>(castedContainer, cloner);
    };
}

StatusCode CopyLinePersistenceLocations::initialize() {
  const StatusCode sc = MicroDSTCommon<GaudiAlgorithm>::initialize();
  if (sc.isFailure()) {
    return sc;
  }

  m_linePersistenceSvc = svc<ILinePersistenceSvc>(m_linePersistenceSvcName.value());
  if (!m_linePersistenceSvc) {
    throw GaudiException("Could not acquire ILinePersistenceSvc", this->name(), StatusCode::FAILURE);
  }

  registerCloner<LHCb::CaloCluster>(m_caloClusterClonerName, m_cloners);
  registerCloner<LHCb::CaloHypo>(m_caloHypoClonerName, m_cloners);
  registerCloner<LHCb::Particle>(m_particleClonerName, m_cloners);
  registerCloner<LHCb::ProtoParticle>(m_protoParticleClonerName, m_cloners);
  // Vertex cloners are specialised with the VertexBase class, but we
  // want to support KeyedContainers of RecVertex, not VertexBase
  registerCloner<LHCb::RecVertex, LHCb::VertexBase>(m_vertexClonerName, m_cloners);
  // Can have a special vertex cloner for locations belonging to full
  // PersistReco lines, with the principle use case being that PR lines get
  // full PVs, including VELO tracks, rather than the 'light' PVs, without VELO
  // tracks, that Turbo lines get
  registerCloner<LHCb::RecVertex, LHCb::VertexBase>(m_turboPPVertexClonerName,
                                                    m_turboPPCloners);

  // Fall back to non-specific cloners if a Turbo++ cloner wasn't specified
  m_turboPPCloners.insert(m_cloners.begin(), m_cloners.end());

  return sc;
}

StatusCode CopyLinePersistenceLocations::execute() {
  if (UNLIKELY(msgLevel(MSG::DEBUG))) {
    debug() << "==> Execute" << endmsg;
  }
  setFilterPassed(true);

  const auto decReports = getIfExists<LHCb::HltDecReports>(m_hltDecReportsLocation.value());
  if (!decReports) {
    return Warning("Could not retrieve HltDecReports from " + m_hltDecReportsLocation.value(),
                   StatusCode::SUCCESS);
  }
  const auto locationsToCopy = m_linePersistenceSvc->locationsToPersist(*decReports, m_linesToCopySet);
  const auto turboPPLocationsToCopy = m_linePersistenceSvc->turboPPLocationsToPersist(*decReports, m_linesToCopySet);
  ILinePersistenceSvc::Locations nonTurboPPLocationsToCopy;
  std::set_difference(locationsToCopy.begin(), locationsToCopy.end(),
                      turboPPLocationsToCopy.begin(), turboPPLocationsToCopy.end(),
                      std::inserter(nonTurboPPLocationsToCopy, nonTurboPPLocationsToCopy.begin()));

  if (msgLevel(MSG::VERBOSE)) {
    if (locationsToCopy.empty()) {
      verbose() << "No locations to copy" << endmsg;
    } else {
      verbose() << "Will attempt to copy the following locations:" << endmsg;
      for (const auto& loc : locationsToCopy) {
        verbose() << "  - " << loc << endmsg;
      }
      if (!turboPPLocationsToCopy.empty()) {
        verbose() << "Out of which these will be cloned with Turbo++ cloners:" << endmsg;
        for (const auto& loc : locationsToCopy) {
          verbose() << "  - " << loc << endmsg;
        }
      }
    }
  }

  // Here we make the implicit assumption that the Turbo++ cloners will
  // clone a superset of the data cloned by the standard cloners.
  this->cloneLocations(turboPPLocationsToCopy, m_turboPPCloners);
  this->cloneLocations(nonTurboPPLocationsToCopy, m_cloners);

  return StatusCode::SUCCESS;
}

void CopyLinePersistenceLocations::cloneLocations(const std::set<std::string>& locations,
                                                  const CopyLinePersistenceLocations::CLIDToCloner& cloners) {
  for (const auto& containerLocation : locations) {
    const auto dataObject = getIfExists<DataObject>(containerLocation);
    if (!dataObject) {
      if (msgLevel(MSG::VERBOSE)) {
        verbose() << "Found no data at location " << containerLocation << endmsg;
      }
      continue;
    }
    const auto classID = dataObject->clID();

    auto it = cloners.find(classID);
    if (it == cloners.end()) {
      if (msgLevel(MSG::VERBOSE)) {
        verbose() << "Cannot clone location " << containerLocation
                  << " with classID " << classID << ", skipping" << endmsg;
      }
      continue;
    }
    it->second(*dataObject);
  }
}

template<typename KeyedObjectToClone, typename ClonerSourceObject/* = KeyedObjectToClone*/>
void CopyLinePersistenceLocations::cloneKeyedContainer(const KeyedContainer<KeyedObjectToClone>& container,
                                                       MicroDST::ICloner<ClonerSourceObject>* cloner) const {
  auto inputLocation = MicroDST::objectLocation(&container);
  if (m_alwaysCreateOutput.value() && container.empty()) {
    auto outputLocation = this->outputTESLocation(inputLocation);
    if (msgLevel(MSG::VERBOSE)) {
      verbose() << "Creating empty container at location " << outputLocation << endmsg;
    }
    put(new KeyedContainer<KeyedObjectToClone>{}, outputLocation);
  } else if (msgLevel(MSG::VERBOSE)) {
    verbose() << "Copying " << container.size() << " objects from container at location "
              << inputLocation << endmsg;
  }

  for (const auto& keyedObject : container) {
    if (msgLevel(MSG::VERBOSE)) {
      verbose() << "Copying " << keyedObject << " with " << cloner->type() << endmsg;
    }
    auto clone = (*cloner)(keyedObject);
    if (!clone) {
      std::stringstream ss;
      ss << "Failed to clone " << keyedObject << " with " << cloner->type() << std::endl;
      Warning(ss.str()).ignore();
    }
  }
}
