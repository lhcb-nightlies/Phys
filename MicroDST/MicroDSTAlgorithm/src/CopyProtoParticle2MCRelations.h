#ifndef COPYPROTOPARTICLE2MCRELATIONS_H 
#define COPYPROTOPARTICLE2MCRELATIONS_H 1

// Include files
// from MicroDST
#include "MicroDST/RelationsFromParticleLocationsClonerAlg.h"
#include <MicroDST/ICloneMCParticle.h>
#include "MicroDST/BindType2ClonerDef.h"
// from DaVinci
#include "Kernel/Particle2MCParticle.h"

/** @class CopyProtoParticle2MCRelations CopyProtoParticle2MCRelations.h
 *
 *  Near-duplicate of the CopyParticle2MCRelations microDST algorithm that
 *  copies weighted LHCb::ProtoParticle to LHCb:MCParticle relations, rather
 *  than weighted LHCb::Particle to LHCb:MCParticle relations.
 *
 *  Another difference is that this algorithm is a specialisation of the
 *  MicroDST::RelationsClonerAlg template class, rather than the
 *  MicroDST::RelationsFromParticleLocationsClonerAlg class that
 *  CopyParticle2MCRelations specialises. With this difference, this class
 *  takes TES locations of relations tables as input, rather than taking
 *  particle locations and trying to deduce the relations table location from
 *  that. It will then take the locations of the associated LHCb::Particle
 *  objects by interrogating the relations table itself.
 */
typedef LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double> PP2MCPTable;
//=============================================================================
template <> struct BindType2Cloner<PP2MCPTable>
{
  typedef LHCb::MCParticle ToType;
  typedef ICloneMCParticle ToCloner;
};
//=============================================================================
template<> struct Defaults<PP2MCPTable>
{
  const static std::string clonerType;
  const static std::string relationsName;
};
const std::string Defaults<PP2MCPTable>::clonerType    = "MCParticleCloner";
//=============================================================================
template<> struct Location<PP2MCPTable>
{
  const static std::string Default;
};
const std::string Location<PP2MCPTable>::Default = "NO DEFAULT LOCATION";
//=============================================================================
typedef MicroDST::RelationsClonerAlg<PP2MCPTable> CopyProtoParticle2MCRelations;
DECLARE_NAMED_ALGORITHM_FACTORY(CopyProtoParticle2MCRelations, CopyProtoParticle2MCRelations)
#endif // COPYPROTOPARTICLE2MCRELATIONS_H
