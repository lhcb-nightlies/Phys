#include "TrackClonerWithClusters.h"

TrackClonerWithClusters::TrackClonerWithClusters( const std::string& type,
                                                          const std::string& name,
                                                          const IInterface* parent )
  : base_class  ( type, name, parent ),
    m_mcPcloner ( NULL )
{
  declareProperty( "ICloneMCParticle",
                   m_mcpClonerName = "MCParticleCloner" );
  declareProperty( "CloneAncestors", m_cloneAncestors = true  );
  declareProperty( "CloneMCLinks",   m_cloneMCLinks   = false );
  declareProperty( "VeloClusters", m_veloClusLoc = LHCb::VeloClusterLocation::Default );
  declareProperty( "TTClusters",   m_ttClusLoc = LHCb::STClusterLocation::TTClusters );
  declareProperty( "ITClusters",   m_itClusLoc = LHCb::STClusterLocation::ITClusters );
  //setProperty( "OutputLevel", 1 );
}

//=============================================================================

StatusCode TrackClonerWithClusters::initialize()
{
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  // Setup incident services
  incSvc()->addListener( this, IncidentType::BeginEvent );

  // MC stuff
  if ( m_cloneMCLinks ) { debug() << "Will clone MC Links" << endmsg; }

  return sc;
}

//=============================================================================

// Method that handles various Gaudi "software events"
void TrackClonerWithClusters::handle ( const Incident& /* incident */ )
{
  // Only one Incident type, so skip type check
  clonedTrackList().clear();
}

//=============================================================================

LHCb::Track* TrackClonerWithClusters::operator() ( const LHCb::Track* track )
{
  return this->clone(track);
}

//=============================================================================

LHCb::Track* TrackClonerWithClusters::clone ( const LHCb::Track* track )
{
  if ( !track )
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Track pointer is NULL !" << endmsg;
    return NULL;
  }

  if ( !track->parent() )
  {
    this->Warning( "Cannot clone a Track with no parent!" ).ignore();
    return NULL;
  }

  // Is this location in the veto list ?
  if ( isVetoed(track) ) { return const_cast<LHCb::Track*>(track); }

  LHCb::Track * cloneTrack = cloneKeyedContainerItem<BasicTrackCloner>(track);

  // Did the cloning work ?
  if ( cloneTrack )
  {

    // If so clone ancestors if required
    cloneTrack->clearAncestors();
    if ( m_cloneAncestors )
    {
      for ( const SmartRef<LHCb::Track>& Tk : track->ancestors() )
      {
        const LHCb::Track * cloneAnTk = this->clone(Tk);
        if ( cloneAnTk ) { cloneTrack->addToAncestors(cloneAnTk); }
      }
    }

    cloneClusters(cloneTrack);

    // Clone MC links ?
    if ( m_cloneMCLinks ) { cloneMCLinks(track,cloneTrack); }

  }

  return cloneTrack;
}

void TrackClonerWithClusters::cloneClusters(const LHCb::Track* track)
{
  const auto allIDs = track->lhcbIDs();
  const auto vClus = getIfExists<LHCb::VeloClusters>(m_veloClusLoc);
  const auto ttClus = getIfExists<LHCb::STClusters>(m_ttClusLoc);
  const auto itClus = getIfExists<LHCb::STClusters>(m_itClusLoc);

  if ( !vClus )
  {
    Warning("Failed to load '" + m_veloClusLoc + "'");
  }
  if ( !ttClus )
  {
    Warning("Failed to load '" + m_ttClusLoc + "'");
  }
  if ( !itClus )
  {
    Warning("Failed to load '" + m_itClusLoc + "'");
  }

  for ( const auto& id : allIDs )
  {
    if ( msgLevel(MSG::VERBOSE) ) { verbose() << "Cloning " << id << endmsg; }

    // Clone by type
    if ( id.isVelo() && vClus )
    {
      const auto * cl = vClus->object(id.veloID());
      if ( cl )
      {
        const LHCb::VeloCluster* cloneCl = cloneKeyedContainerItem<BasicVeloClusterCloner>(cl);
        if (!cloneCl)
        {
          Warning( "Failed to clone a Velo cluster. Activate debug for details" ).ignore();
          if ( msgLevel(MSG::DEBUG) ) debug() << "Velo cluster not cloned : " << id << endmsg;
        }
      }
      else
      {
        Warning( "Failed to locate a Velo cluster. Activate debug for details" ).ignore();
        if ( msgLevel(MSG::DEBUG) ) debug() << "Unknown Velo cluster : " << id << endmsg;
      }
    }
    else if ( id.isTT() && ttClus )
    {
      const auto * cl = ttClus->object(id.stID());
      if ( cl )
      {
        const LHCb::STCluster* cloneCl = cloneKeyedContainerItem<BasicSTClusterCloner>(cl);
        if (!cloneCl)
        {
          Warning( "Failed to clone a TT cluster. Activate debug for details" ).ignore();
          if ( msgLevel(MSG::DEBUG) ) debug() << "TT cluster not cloned : " << id << endmsg;
        }
      }
      else
      {
        Warning( "Failed to locate a TT cluster. Activate debug for details" ).ignore();
        if ( msgLevel(MSG::DEBUG) ) debug() << "Unknown TT cluster : " << id << endmsg;
      }
    }
    else if ( id.isIT() && itClus )
    {
      const auto * cl = itClus->object(id.stID());
      if ( cl )
      {
        const LHCb::STCluster* cloneCl = cloneKeyedContainerItem<BasicSTClusterCloner>(cl);
        if (!cloneCl)
        {
          Warning( "Failed to clone a IT cluster. Activate debug for details" ).ignore();
          if ( msgLevel(MSG::DEBUG) ) debug() << "IT cluster not cloned : " << id << endmsg;
        }
      }
      else
      {
        Warning( "Failed to locate a IT cluster. Activate debug for details" ).ignore();
        if ( msgLevel(MSG::DEBUG) ) debug() << "Unknown IT cluster : " << id << endmsg;
      }
    }
    else if ( id.isOT() )
    {
      // OT IDs encode cluster information, so no need to clone the clusters
      if ( msgLevel(MSG::VERBOSE) ) { verbose() << "Ignoring OT cluster" << endmsg; }
      continue;
    }
    else if ( id.isMuon() )
    {
      // We can't handle MUON clusters
      if ( msgLevel(MSG::VERBOSE) ) { verbose() << "Ignoring MUON cluster" << endmsg; }
      continue;
    }
    else
    {
      Warning( "Unknown LHCbID. Activate debug for details" ).ignore();
      if ( msgLevel(MSG::DEBUG) ) debug() << "Unknown LHCbID type : " << id << endmsg;
    }
  }
}

//=============================================================================

void TrackClonerWithClusters::cloneMCLinks( const LHCb::Track* track,
                                const LHCb::Track* cloneTrack )
{

  // Linker typedefs
  typedef LinkerWithKey<LHCb::MCParticle,LHCb::Track>  Linker;
  typedef LinkerTool<LHCb::Track,LHCb::MCParticle>     Asct;
  typedef Asct::DirectType                             Table;

  // has this clone already been done
  if ( std::find( clonedTrackList().begin(),
                  clonedTrackList().end(),
                  cloneTrack ) == clonedTrackList().end() )
  {

    // location in TES of original tracks
    const std::string tkLoc = objectLocation( track->parent() );

    // try and load the linker tool
    Asct linker = Asct( evtSvc(), tkLoc );
    const Table * table = linker.direct();

    // If we found a table, try and use it
    if ( table )
    {
      // location in TES of cloned tracks
      const std::string cloneLoc = objectLocation( cloneTrack->parent() );

      // Create a new linker for the cloned tracks
      Linker clonedLinks( evtSvc(), msgSvc(), cloneLoc );

      // Loop over relations for original track
      for ( const auto& Rentry : table->relations(track) )
      {
        // get cloned MCParticle
        const LHCb::MCParticle * clonedMCP = mcPCloner()( Rentry.to() );
        if ( clonedMCP )
        {
          // if cloning worked, fill relation in linker with original weight
          clonedLinks.link( cloneTrack, clonedMCP, Rentry.weight() );
        }
      }
    }

    // save in the list
    clonedTrackList().push_back(cloneTrack);

  }

}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( TrackClonerWithClusters )

//=============================================================================
