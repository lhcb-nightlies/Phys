// $Id: CaloClusterCloner.cpp,v 1.1 2010-08-13 14:33:57 jpalac Exp $

// from Gaudi
#include "GaudiKernel/ToolFactory.h"

// local
#include "CaloClusterCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloClusterCloner
//
// 2008-04-01 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================

CaloClusterCloner::CaloClusterCloner( const std::string& type,
                                      const std::string& name,
                                      const IInterface* parent )
  : base_class ( type, name, parent )
{
  declareProperty( "CloneEntriesNeuP", m_cloneEntriesNeuP = false );
  declareProperty( "CloneEntriesChP",  m_cloneEntriesChP  = false );
  declareProperty( "CloneEntriesAlways",  m_cloneEntriesAlways  = false );
  declareProperty( "UseStatusMask",    m_mask = true ); //  true : only store the masked digits
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode CaloClusterCloner::initialize()
{
  const auto sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  m_caloDigitCloner = tool<ICloneCaloDigit>("CaloDigitCloner",this->parent());

  return sc;
}

//=============================================================================

LHCb::CaloCluster* 
CaloClusterCloner::operator() ( const LHCb::CaloCluster* cluster )
{
  return this->clone(cluster);
}

//=============================================================================

LHCb::CaloCluster* 
CaloClusterCloner::clone( const LHCb::CaloCluster* cluster,
                          const LHCb::Particle * parent )
{
  if ( !cluster )
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "CaloCluster pointer is NULL !" << endmsg;
    return nullptr;
  }

  if ( !cluster->parent() )
  {
    this->Warning( "Cannot clone a CaloCluster with no parent !" ).ignore();
    return nullptr;
  }

  // Is this object in the veto list ?
  if ( isVetoed(cluster) ) { return const_cast<LHCb::CaloCluster*>(cluster); }

  // has this item already been cloned ? If so, just return it without modification.
  auto cloneCluster = getStoredClone<LHCb::CaloCluster>(cluster);
  if ( cloneCluster ) 
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Found pre-cloned cluster with " 
              << cloneCluster->entries().size() << " entries" << endmsg;
  }
  const bool newClone = !cloneCluster;
  //const bool newClone = true;

  // if we get here, we need to make a clone
  cloneCluster = cloneKeyedContainerItem<BasicCaloClusterCloner>(cluster);
  if ( !cloneCluster ) return cloneCluster;

  // If this is a new clone, wipe the containers
  if ( newClone )
  {
    cloneCluster->entries().clear();
  }

  // Check calo status
  const bool isCaloNeu = isPureNeutralCalo(parent);
  const bool isCharged = isChargedBasic(parent);

  // Do we require digits ?
  const bool reqDigits = ( m_cloneEntriesAlways ||
                           ( isCaloNeu && m_cloneEntriesNeuP ) ||
                           ( isCharged && m_cloneEntriesChP  ) );
  const bool hasDigits  = !cloneCluster->entries().empty();
  const bool fillDigits = reqDigits && ( newClone || !hasDigits );
  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "RequireDigits = " << reqDigits
              << " hasDigits = " << hasDigits
              << " fillDigits = " << fillDigits << endmsg;

  // Clone the entries ?
  if ( fillDigits )
  {
    if ( msgLevel(MSG::VERBOSE) ) verbose() << "Cloning entries" << endmsg;
    const auto & entries = cluster->entries();
    if ( !entries.empty() )
    {
      std::vector<LHCb::CaloClusterEntry> clonedEntries;
      clonedEntries.reserve( entries.size() );
      for ( const auto& e : entries )
      {
        // check status
        if ( m_mask && ( e.status() & m_status ) == 0 ) continue;
        // clone the digit
        clonedEntries.emplace_back(e);
        auto & entryClone = clonedEntries.back();
        const auto digitClone = (*m_caloDigitCloner)( e.digit() );
        entryClone.setDigit(digitClone);
      }
      cloneCluster->setEntries( clonedEntries );
    }
  } else if ( msgLevel(MSG::VERBOSE) )
  { verbose() << " NO Need to fill Digits" << endmsg; }

  // return the clone
  return cloneCluster;
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( CaloClusterCloner )

//=============================================================================
