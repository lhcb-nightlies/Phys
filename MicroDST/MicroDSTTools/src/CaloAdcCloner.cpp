// $Id: CaloAdcCloner.cpp,v 1.1 2017-06-15 14:33:57 rvazquez Exp $

// from Gaudi
#include "GaudiKernel/ToolFactory.h"

// local
#include "CaloAdcCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloAdcCloner
//
// 2017-06-15 : Ricardo Vazquez Gomez
//-----------------------------------------------------------------------------

//=============================================================================

CaloAdcCloner::CaloAdcCloner( const std::string& type,
                              const std::string& name,
                              const IInterface* parent )
  : base_class ( type, name , parent )
{
  //setProperty( "OutputLevel", 1 );
}

//=============================================================================

LHCb::CaloAdc* 
CaloAdcCloner::operator() (const LHCb::CaloAdc* adc)
{
  return this->clone(adc);
}

//=============================================================================

LHCb::CaloAdc* CaloAdcCloner::clone(const LHCb::CaloAdc* adc)
{
  if ( !adc )
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "CaloAdc pointer is NULL !" << endmsg;
    return NULL;
  }

  if ( !adc->parent() )
  {
    this->Warning( "Cannot clone a CaloAdc with no parent !" ).ignore();
    return NULL;
  }

  // Is this object in the veto list ?
  if ( isVetoed(adc) ) { return const_cast<LHCb::CaloAdc*>(adc); }

  LHCb::CaloAdc* clone =
    cloneKeyedContainerItem<BasicCaloAdcCloner>(adc);
  if ( !clone ) return clone;

  return clone;
}

//=============================================================================

CaloAdcCloner::~CaloAdcCloner() {}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( CaloAdcCloner )

//=============================================================================
