
// from Gaudi
#include "GaudiKernel/ToolFactory.h"

// from LHCb
#include "Event/CaloHypo.h"
#include "Event/CaloDigit.h"
#include "Event/CaloCluster.h"

// MicroDST
#include "MicroDST/ICloneCaloCluster.h"

// local
#include "CaloHypoCloner.h"

// STL
#include <sstream>

//-----------------------------------------------------------------------------
// Implementation file for class : CaloHypoCloner
//
// 2008-04-01 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================

CaloHypoCloner::CaloHypoCloner( const std::string& type,
                                const std::string& name,
                                const IInterface* parent )
  : base_class ( type, name, parent  )
{
  declareProperty( "CloneHypos",        m_cloneHypos        = true  );
  declareProperty( "CloneClustersNeuP", m_cloneClustersNeuP = true  );
  declareProperty( "CloneDigitsNeuP",   m_cloneDigitsNeuP   = true  );
  declareProperty( "CloneClustersChP",  m_cloneClustersChP  = false );
  declareProperty( "CloneDigitsChP",    m_cloneDigitsChP    = false );
  declareProperty( "CloneClustersAlways", m_cloneClustersAlways = false );
  declareProperty( "CloneDigitsAlways",   m_cloneDigitsAlways   = false );
  declareProperty( "ICloneMCParticle",
                   m_mcpClonerName = "MCParticleCloner" );
  declareProperty( "CloneMCLinks",   m_cloneMCLinks = false );
  declareProperty( "SelectClusters", m_selClusters  = false );
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode CaloHypoCloner::initialize()
{
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  // Setup incident services
  incSvc()->addListener( this, IncidentType::BeginEvent );

  m_caloClusterCloner = tool<ICloneCaloCluster>( m_caloClusterClonerName,
                                                 this->parent() );
  m_caloDigitCloner = tool<ICloneCaloDigit>("CaloDigitCloner",this->parent());

  if ( m_cloneMCLinks ) { debug() << "Will clone MC Links" << endmsg; }
 
  return sc;
}

//=============================================================================

// Method that handles various Gaudi "software events"
void CaloHypoCloner::handle ( const Incident& /* incident */ )
{
  // Only one Incident type, so skip type check
  clonedHypoList().clear();
}

//=============================================================================

LHCb::CaloHypo* CaloHypoCloner::operator() ( const LHCb::CaloHypo* hypo )
{
  return this->clone(hypo);
}

//=============================================================================

LHCb::CaloHypo* CaloHypoCloner::clone( const LHCb::CaloHypo* hypo,
                                       const LHCb::Particle * parent )
{
  if ( !hypo )
  {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "CaloHypo pointer is NULL !" << endmsg;
    return nullptr;
  }

  if ( !hypo->parent() )
  {
    this->Warning( "Cannot clone a CaloHypo with no parent !" ).ignore();
    return nullptr;
  }

  // Is this object in the veto list ?
  if ( isVetoed(hypo) ) { return const_cast<LHCb::CaloHypo*>(hypo); }
  
  // has this item already been cloned ? If so, just return it without modification.
  auto cloneHypo = getStoredClone<LHCb::CaloHypo>(hypo);
  const bool newClone = !cloneHypo;
  //const bool newClone = true; // force for tests only
  if ( msgLevel(MSG::VERBOSE) ) verbose() << "New Clone = " << newClone << endmsg;

  // if we get here, we need to make a clone
  cloneHypo = cloneKeyedContainerItem<BasicCaloHypoCloner>(hypo);
  if ( !cloneHypo ) return cloneHypo;

  // If this is a new clone, wipe the containers
  if ( newClone )
  {
    cloneHypo->clearHypos();
    cloneHypo->clearDigits();
    cloneHypo->clearClusters();
  }

  // Check calo status
  const bool isCaloNeu = isPureNeutralCalo(parent);
  const bool isCharged = isChargedBasic(parent);

  // Clone linked hypos
  if ( newClone )
  {
    if ( m_cloneHypos )
    {
      if ( !hypo->hypos().empty() )
      {
        SmartRefVector<LHCb::CaloHypo> clonedHypos;
        for ( const auto& h :hypo->hypos() )
        {
          if ( h.target() )
          {
            LHCb::CaloHypo * _hypo = clone(h,parent);
            if ( _hypo ) { clonedHypos.push_back(_hypo); }
          }
          else
          {
            std::ostringstream mess;
            mess << "CaloHypo in '" << tesLocation(hypo)
                 << "' has null hypo SmartRef -> skipping";
            Warning( mess.str(), StatusCode::SUCCESS ).ignore();
          }
        }
        cloneHypo->setHypos(clonedHypos);
      }
    }
  }

  // Do we require digits ?
  const bool reqDigits = ( m_cloneDigitsAlways ||
                           ( isCaloNeu && m_cloneDigitsNeuP ) ||
                           ( isCharged && m_cloneDigitsChP  ) );
  const bool hasDigits  = !cloneHypo->digits().empty();
  const bool fillDigits = reqDigits && ( newClone || !hasDigits );
  if ( msgLevel(MSG::VERBOSE) ) 
    verbose() << "RequireDigits = " << reqDigits
              << " hasDigits = " << hasDigits
              << " fillDigits = " << fillDigits << endmsg;

  if ( fillDigits )
  {
    if ( msgLevel(MSG::VERBOSE) ) verbose() << " Need to fill Digits" << endmsg;
    if ( !hypo->digits().empty() )
    {
      SmartRefVector<LHCb::CaloDigit> clonedDigits;
      for ( const auto& dig : hypo->digits() )
      {
        if ( dig.target() )
        {
          LHCb::CaloDigit * _digit = (*m_caloDigitCloner)(dig);
          if ( _digit ) { clonedDigits.push_back( _digit ); }
        }
        else
        {
          std::ostringstream mess;
          mess << "CaloHypo in '" << tesLocation(hypo)
               << "' has null digit SmartRef -> skipping";
          Warning( mess.str(), StatusCode::SUCCESS ).ignore();
        }
      }
      cloneHypo->setDigits(clonedDigits);
    }
  } else if ( msgLevel(MSG::VERBOSE) ) 
  { verbose() << " NO Need to fill Digits" << endmsg; }

  // Do we require clusters ?
  const bool reqClus = ( m_cloneClustersAlways ||
                         ( isCaloNeu && m_cloneClustersNeuP ) ||
                         ( isCharged && m_cloneClustersChP  ) );
  const bool hasClus = !cloneHypo->clusters().empty();
  const bool fillClus = reqClus && ( newClone || !hasClus );
  if ( msgLevel(MSG::VERBOSE) ) 
    verbose() << "RequireClus = " << reqClus
              << " hasClus = " << hasClus
              << " fillClus = " << fillClus << endmsg;

  if ( fillClus )
  {
    if ( msgLevel(MSG::VERBOSE) ) verbose() << " Need to fill Clusters" << endmsg;
    if ( !hypo->clusters().empty() )
    {
      SmartRefVector<LHCb::CaloCluster> clonedClusters;
      if ( isCaloNeu && m_selClusters )
      {
        // select clusters precisely for neutrals
        using ClusPair = std::pair<const LHCb::CaloCluster*,const LHCb::CaloCluster*>;
        std::vector<ClusPair> selClusters;
        // 2- in case of merged pi0 -  store the associated split-clusters 
        if ( hypo->hypothesis() == LHCb::CaloHypo::Pi0Merged )
        {
          for ( const auto& split : hypo->hypos() )
          {
            const auto splitcl = LHCb::CaloAlgUtils::ClusterFromHypo(split);
            const auto clonecl = m_caloClusterCloner->clone(splitcl,parent);
            if ( clonecl ) { selClusters.emplace_back(splitcl,clonecl); }
          }
        }
        // 3- store the main cluster
        const auto maincl      = LHCb::CaloAlgUtils::ClusterFromHypo(hypo);
        const auto clonemaincl = m_caloClusterCloner->clone(maincl,parent);
        if ( clonemaincl ) { selClusters.emplace_back(maincl,clonemaincl); }
        // Update stored cluster refs
        for ( const auto& clus : hypo->clusters() )
        {
          const auto fcl = 
            std::find_if( selClusters.begin(), selClusters.end(),
                          [&clus]( const auto& i )
                          { return ( clus.target() == i.first ); } );
          if ( fcl != selClusters.end() )
          {
            clonedClusters.push_back( fcl->second );
          }
        }
      }
      else
      {
        // select everything
        for ( const auto& clus : hypo->clusters() )
        {
          if ( clus.target() )
          {
            auto cl = m_caloClusterCloner->clone(clus,parent);
            if ( cl ) { clonedClusters.push_back( cl ); }
          }
          else
          {
            std::ostringstream mess;
            mess << "CaloHypo in '" << tesLocation(hypo)
                 << "' has null cluster SmartRef -> skipping";
            Error( mess.str() ).ignore();
          }
        }
      }
      cloneHypo->setClusters(clonedClusters);
    }
  } else if ( msgLevel(MSG::VERBOSE) ) 
  { verbose() << " NO Need to fill Clusters" << endmsg; }

  // clone MC Links
  if ( newClone && m_cloneMCLinks ) { cloneMCLinks(hypo,cloneHypo); }

  return cloneHypo;
}

//=============================================================================

void CaloHypoCloner::cloneMCLinks( const LHCb::CaloHypo* hypo,
                                   const LHCb::CaloHypo* cloneHypo )
{

  // Linker typedefs
  typedef LinkerWithKey<LHCb::MCParticle,LHCb::CaloHypo>  Linker;
  typedef LinkerTool<LHCb::CaloHypo,LHCb::MCParticle>     Asct;
  typedef Asct::DirectType                                Table;

  // has this clone already been done
  if ( std::find( clonedHypoList().begin(),
                  clonedHypoList().end(),
                  cloneHypo ) == clonedHypoList().end() )
  {

    // location in TES of original hypo
    const std::string hypoLoc = objectLocation( hypo->parent() );

    // try and load the linker tool
    Asct linker = Asct( evtSvc(), hypoLoc );
    const Table * table = linker.direct();

    // If we found a table, try and use it
    if ( table )
    {
      // location in TES of cloned hypos
      const std::string cloneLoc = objectLocation( cloneHypo->parent() );

      // Create a new linker for the cloned tracks
      Linker clonedLinks( evtSvc(), msgSvc(), cloneLoc );

      // Loop over relations for original hypo
      for ( const auto& Rentry : table->relations(hypo) )
      {
        // get cloned MCParticle
        const LHCb::MCParticle * clonedMCP = mcPCloner()( Rentry.to() );
        if ( clonedMCP )
        {
          // if cloning worked, fill relation in linker with original weight
          clonedLinks.link( cloneHypo, clonedMCP, Rentry.weight() );
        }
      }

    }

    // save in the list
    clonedHypoList().push_back(cloneHypo);

  }

}

//=============================================================================

DECLARE_TOOL_FACTORY( CaloHypoCloner )

//=============================================================================
